from shapely import Polygon

from bbox import bounding_box_to_polygon
from kimiworker import ContentLabel


def get_page_boundary_labels(box_polygon: Polygon):
    """
    Determines if a given polygon is primarily within the predefined header or footer regions of a page.

    The function calculates the intersection area of the given polygon with both header and footer regions
    to see if a significant portion (>90%) of the polygon lies within these areas, thereby classifying it
    as part of the header or footer.

    Args:
        - box_polygon (Polygon): A polygon representing the bounding box of text or other content on the page.

    Returns:
        - List[ContentLabel]: A list containing labels for content identified as part of the header or footer,
        with each label including the region's name and a confidence score.
    """
    footer_height = 0.09
    footer_region_polygon = bounding_box_to_polygon(
        {"top": 1 - footer_height, "left": 0, "right": 1, "bottom": 1}
    )

    header_height = 0.09
    header_region_polygon = bounding_box_to_polygon(
        {"top": 0, "left": 0, "right": 1, "bottom": header_height}
    )

    labels: list[ContentLabel] = []
    # TODO: Lowest text blocks on the page could be in footer
    # This implementation is probably too crude, so needs some refinement
    footer_intersection = (
        box_polygon.intersection(footer_region_polygon).area / box_polygon.area
    )
    if footer_intersection >= 0.90:
        labels.append(
            {
                "name": "footer",
                "confidence": 0.75,
            }
        )

    # TODO: Highest text blocks on the page could be in footer
    # This implementation is probably too crude, so needs some refinement
    header_intersection = (
        box_polygon.intersection(header_region_polygon).area / box_polygon.area
    )
    if header_intersection >= 0.90:
        labels.append(
            {
                "name": "header",
                "confidence": 0.75,
            }
        )

    return labels

from kimiworker import ContentAttribute, ContentLabel

from validations import (
    contains_month,
    contains_year,
    ends_with_colon,
    ends_with_period,
    ends_with_semicolon,
    ends_with_year,
    has_list_item_structure,
    is_lowercase,
    is_numeric,
    is_single_word,
    is_uppercase,
    starts_numeric,
    starts_with_capital,
    starts_with_roman_numeral,
    starts_with_single_letter,
)


def get_text_labels(text: ContentAttribute | None):
    """Get text labels for content attribute"""

    labels: list[ContentLabel] = []

    if is_numeric(text):
        labels.append(
            {
                "name": "is_numeric",
                "confidence": 1.0,
            }
        )

    if starts_numeric(text):
        labels.append(
            {
                "name": "starts_numeric",
                "confidence": 1.0,
            }
        )

    if starts_with_capital(text):
        labels.append(
            {
                "name": "starts_with_cap",
                "confidence": 1.0,
            }
        )

    if is_single_word(text):
        labels.append(
            {
                "name": "single_word",
                "confidence": 1.0,
            }
        )

    if is_uppercase(text):
        labels.append(
            {
                "name": "uppercase",
                "confidence": 1.0,
            }
        )

    if is_lowercase(text):
        labels.append(
            {
                "name": "lowercase",
                "confidence": 1.0,
            }
        )

    if ends_with_colon(text):
        labels.append(
            {
                "name": "ends_with_colon",
                "confidence": 1.0,
            }
        )

    if ends_with_semicolon(text):
        labels.append(
            {
                "name": "ends_with_semicolon",
                "confidence": 1.0,
            }
        )

    if ends_with_period(text):
        labels.append(
            {
                "name": "ends_with_period",
                "confidence": 1.0,
            }
        )

    if has_list_item_structure(text):
        labels.append(
            {
                "name": "list_item_like",
                "confidence": 0.9,
            }
        )

    if contains_year(text):
        labels.append(
            {
                "name": "contains_year",
                "confidence": 75,
            }
        )

    if ends_with_year(text):
        labels.append(
            {
                "name": "ends_with_year",
                "confidence": 0.9,
            }
        )

    if contains_month(text):
        labels.append(
            {
                "name": "contains_month",
                "confidence": 1.0,
            }
        )

    if starts_with_roman_numeral(text):
        labels.append(
            {
                "name": "starts_with_roman_numeral",
                "confidence": 1.0,
            }
        )

    if starts_with_single_letter(text):
        labels.append(
            {
                "name": "starts_with_single_letter",
                "confidence": 1.0,
            }
        )

    return labels

import numpy as np
import pytest
from colors import (
    detect_background_color,
    get_top_colors,
    is_black_strict,
    rgb_to_hex,
    uniform_quantize_image,
)


def test_detect_background_color_most_common():
    """Test background detection with single dominant color"""
    image = np.zeros((100, 100, 3), dtype=np.uint8)
    image[:] = (255, 0, 0)  # Red color
    background_color = detect_background_color(image)
    assert np.array_equal(background_color, [255, 0, 0])


def test_detect_background_color_average():
    """Test background detection with two equal color regions"""
    image = np.zeros((100, 100, 3), dtype=np.uint8)
    image[:50, :] = (255, 0, 0)  # Red color in the top half
    image[50:, :] = (0, 255, 0)  # Green color in the bottom half
    background_color = detect_background_color(image)
    assert np.array_equal(background_color, [127, 127, 0])


def test_detect_background_color_unique():
    """Test background detection with single unique color"""
    image = np.zeros((100, 100, 3), dtype=np.uint8)
    image[:] = (123, 45, 67)
    background_color = detect_background_color(image)
    assert np.array_equal(background_color, [123, 45, 67])


def test_detect_background_color_white_background():
    """Test background detection with white background"""
    image = np.zeros((100, 100, 3), dtype=np.uint8)
    image[:] = (255, 255, 255)
    background_color = detect_background_color(image)
    assert np.array_equal(background_color, [255, 255, 255])


def test_detect_background_color_black_background():
    """Test background detection with black background"""
    image = np.zeros((100, 100, 3), dtype=np.uint8)
    image[:] = (0, 0, 0)
    background_color = detect_background_color(image)
    assert np.array_equal(background_color, [0, 0, 0])


def test_quantize_valid_input():
    """Test image quantization with valid input parameters"""
    test_img = np.random.randint(0, 256, (100, 100, 3), dtype=np.uint8)
    quantized_img = uniform_quantize_image(test_img, colors=8)
    assert quantized_img.shape == test_img.shape
    assert quantized_img.dtype == np.uint8
    unique_colors = {tuple(color) for color in quantized_img.reshape(-1, 3)}
    assert len(unique_colors) <= 8


def test_quantize_invalid_color_count():
    """Test quantization with invalid number of colors"""
    test_img = np.random.randint(0, 256, (100, 100, 3), dtype=np.uint8)
    with pytest.raises(ValueError):
        uniform_quantize_image(test_img, colors=-1)


def test_quantize_non_cube_colors():
    """Test quantization with non-cube number of colors"""
    test_img = np.random.randint(0, 256, (100, 100, 3), dtype=np.uint8)
    with pytest.raises(ValueError):
        uniform_quantize_image(test_img, colors=20)


def test_quantize_non_3d_input():
    """Test quantization with invalid 2D input"""
    test_img = np.random.randint(0, 256, (100, 100), dtype=np.uint8)
    with pytest.raises(ValueError):
        uniform_quantize_image(test_img, colors=8)


def test_quantize_preserves_max_min():
    """Test quantization preserves black and white values"""
    test_img = np.array([[[0, 0, 0], [255, 255, 255]]], dtype=np.uint8)
    quantized_img = uniform_quantize_image(test_img, colors=8)
    assert np.all(np.isin([0, 255], quantized_img))


def test_rgb_to_hex_valid_input():
    """Test RGB to hex conversion with various color values"""
    assert rgb_to_hex(np.array([255, 0, 0])) == "#ff0000"
    assert rgb_to_hex(np.array([0, 255, 0])) == "#00ff00"
    assert rgb_to_hex(np.array([0, 0, 255])) == "#0000ff"
    assert rgb_to_hex(np.array([255, 255, 255])) == "#ffffff"
    assert rgb_to_hex(np.array([0, 0, 0])) == "#000000"
    assert rgb_to_hex(np.array([123, 222, 111])) == "#7bde6f"


def test_rgb_to_hex_invalid_input_values():
    """Test RGB to hex conversion with out-of-range values"""
    with pytest.raises(ValueError):
        rgb_to_hex(np.array([256, 0, 0]))
    with pytest.raises(ValueError):
        rgb_to_hex(np.array([-1, 0, 0]))


def test_rgb_to_hex_invalid_input_shapes():
    """Test RGB to hex conversion with invalid array shapes"""
    with pytest.raises(ValueError):
        rgb_to_hex(np.array([255, 255]))
    with pytest.raises(ValueError):
        rgb_to_hex(np.array([255, 255, 255, 255]))


def test_get_top_colors_basic():
    """Test basic functionality with a simple RGB image"""
    img = np.array(
        [[[255, 0, 0], [0, 255, 0]], [[0, 0, 255], [255, 0, 0]]], dtype=np.uint8
    )
    colors, counts = get_top_colors(img)
    assert np.array_equal(colors[0], [255, 0, 0])  # Most frequent color
    assert counts[0] == 2  # Count of most frequent color


def test_get_top_colors_invalid_input():
    """Test handling of invalid input dimensions"""
    with pytest.raises(ValueError):
        get_top_colors(np.array([[1, 2], [3, 4]]))  # 2D array


def test_get_top_colors_amount():
    """Test requesting specific number of colors"""
    img = np.array(
        [[[255, 0, 0], [0, 255, 0]], [[0, 0, 255], [128, 128, 128]]], dtype=np.uint8
    )
    colors, counts = get_top_colors(img, amount=2)
    assert len(colors) == 2
    assert len(counts) == 2


def test_get_top_colors_uniform():
    """Test with uniform color image"""
    img = np.full((3, 3, 3), [100, 100, 100], dtype=np.uint8)
    colors, counts = get_top_colors(img)
    assert np.array_equal(colors[0], [100, 100, 100])
    assert counts[0] == 9  # 3x3 image = 9 pixels


def test_get_top_colors_empty():
    """Test with empty/zero image"""
    img = np.zeros((2, 2, 3), dtype=np.uint8)
    colors, counts = get_top_colors(img)
    assert np.array_equal(colors[0], [0, 0, 0])
    assert counts[0] == 4  # 2x2 image = 4 pixels


def test_is_black_strict_true():
    """Test function correctly identifies black colors"""
    assert is_black_strict(np.array([0, 0, 0]))
    assert is_black_strict(np.array([30, 30, 30]))
    assert is_black_strict(
        np.array([45, 45, 45]), brightness_threshold=60, max_channel_value=50
    )


def test_is_black_strict_false():
    """Test function correctly rejects non-black colors"""
    assert not is_black_strict(np.array([100, 100, 100]))
    assert not is_black_strict(np.array([255, 255, 255]))
    assert not is_black_strict(np.array([60, 0, 0]))


def test_is_black_strict_custom_thresholds():
    """Test function respects custom threshold parameters"""
    color = np.array([70, 70, 70])
    assert not is_black_strict(color)  # False with default thresholds
    assert is_black_strict(
        color, brightness_threshold=100, max_channel_value=80
    )  # True with relaxed thresholds


def test_is_black_strict_bright_channel():
    """Test function rejects colors with any bright channel"""
    assert not is_black_strict(np.array([200, 20, 20]))
    assert not is_black_strict(np.array([20, 200, 20]))
    assert not is_black_strict(np.array([20, 20, 200]))


def test_is_black_strict_edge_cases():
    """Test function handles edge cases correctly"""
    assert is_black_strict(np.array([50, 50, 50]))  # At max_channel_value
    assert not is_black_strict(np.array([51, 51, 51]))  # Just above max_channel_value
    assert is_black_strict(
        np.array([0, 0, 0]), brightness_threshold=0
    )  # Pure black always true

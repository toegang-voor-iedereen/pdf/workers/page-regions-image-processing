from itertools import groupby, pairwise, product
import operator
from typing import Generator


def points_to_grid_coordinates_by_row(points: list[tuple[int, int]]):
    """
    Convert points to grid coordinates grouped by row.

    Args:
        points (list[tuple[int, int]]): List of points.

    Returns:
        generator: Generator of unique grid coordinates grouped by row.
    """
    # Get all unique x and y coordinates from the list of points
    x_axis = set(point[0] for point in points if point is not None and point[0] >= 0)
    y_axis = set(point[1] for point in points if point is not None and point[1] >= 0)

    # Generate all possible grid coordinates by taking the Cartesian product of x and y coordinates
    # Convert to a set to eliminate duplicates, then back to a list for sorting
    grid_coordinates = list(set(product(x_axis, y_axis)))

    # Sort the grid coordinates by y, then by x, and group by y coordinate
    sorted_grid_coordinates = sorted(grid_coordinates, key=lambda k: (k[1], k[0]))
    return (
        list(group)
        for _, group in groupby(sorted_grid_coordinates, key=operator.itemgetter(1))
    )


def pairwise_grid_iterator(
    grid_coordinates_by_row: Generator[list[tuple[int, int]], None, None],
):
    """
    Iterate over grid coordinates grouped by rows, yielding details for adjacent pairs of points within each row.

    Args:
        grid_coordinates_by_row (Generator[list[tuple[int, int]], None, None]): Generator yielding
        lists of (x, y) coordinate tuples for each grid row.

    Yields:
        tuple: Contains:
            - i (int): Index of current pair within row
            - left (int): X-coordinate of current point
            - top (int): Y-coordinate of current point
            - right (int): X-coordinate of next point in row
            - bottom (int): Y-coordinate from first point in next row
            - current_row (list[tuple[int, int]]): Full current row
            - next_row (list[tuple[int, int]]): Full next row
    """
    for current_row, next_row in pairwise(grid_coordinates_by_row):
        sorted_row = sorted(current_row)
        for i in range(len(sorted_row) - 1):  # -1 because we need pairs
            current_point = sorted_row[i]
            next_point = sorted_row[i + 1]
            left = current_point[0]
            right = next_point[0]  # Use x-coordinate of next point
            top = current_point[1]
            bottom = next_row[0][1]
            yield (i, left, top, right, bottom, current_row, next_row)

import pytest
import numpy as np
import cv2
from image import (
    grayscale_image,
    blur_image,
    threshold_image,
    dilate_image,
    find_contours,
    draw_filled_contours,
    canny_edge_image,
    highlight_bold_regions,
    get_bold_regions,
    load_image,
    get_image_dimensions,
    get_image_contours,
    process_image,
)


@pytest.fixture
def sample_image():
    return np.ones((100, 100, 3), dtype=np.uint8) * 255


@pytest.fixture
def sample_gray_image():
    return np.ones((100, 100), dtype=np.uint8) * 255


def test_grayscale_image__rgb_to_grayscale(sample_image):
    # Test RGB to grayscale conversion
    result = grayscale_image(sample_image)
    assert result.shape == (100, 100)


def test_grayscale_image__grayscale_input(sample_gray_image):
    # Test grayscale input
    result = grayscale_image(sample_gray_image)
    assert result.shape == (100, 100)


def test_grayscale_image__single_channel_rgb_input(sample_gray_image):
    # Test single channel RGB input
    single_channel = np.expand_dims(sample_gray_image, axis=2)
    result = grayscale_image(single_channel)
    assert result.shape == (100, 100, 1)


def test_blur_image(sample_image):
    result = blur_image(sample_image)
    assert result.shape == sample_image.shape

    # Test with different kernel size and sigma
    result = blur_image(sample_image, (5, 5), 1.5)
    assert result.shape == sample_image.shape


def test_threshold_image(sample_gray_image):
    result = threshold_image(sample_gray_image)
    assert result.shape == sample_gray_image.shape
    assert np.any(np.isin(result, [0, 255]))  # Check binary output


def test_dilate_image(sample_gray_image):
    result = dilate_image(sample_gray_image)
    assert result.shape == sample_gray_image.shape

    # Test with different kernel parameters
    result = dilate_image(sample_gray_image, cv2.MORPH_ELLIPSE, (3, 3))
    assert result.shape == sample_gray_image.shape


def test_find_contours(sample_gray_image):
    # Create an image with a simple shape
    test_image = np.zeros((100, 100), dtype=np.uint8)
    cv2.rectangle(test_image, (25, 25), (75, 75), 255, -1)

    contours = find_contours(test_image)
    assert len(contours) == 1  # Should find one contour


def test_draw_filled_contours(sample_image):
    # Create a test contour
    test_contour = np.array([[[25, 25]], [[75, 25]], [[75, 75]], [[25, 75]]])
    result = draw_filled_contours(sample_image, [test_contour])
    assert result.shape == sample_image.shape


def test_canny_edge_image(sample_gray_image):
    result = canny_edge_image(sample_gray_image)
    assert result.shape == sample_gray_image.shape

    # Test with different thresholds
    result = canny_edge_image(sample_gray_image, 50, 150)
    assert result.shape == sample_gray_image.shape


def test_highlight_bold_regions(sample_image):
    result = highlight_bold_regions(sample_image)
    assert result.shape == (100, 100)


def test_get_bold_regions(sample_image):
    result = get_bold_regions(sample_image)
    assert isinstance(result, list)


def test_load_image(tmp_path):
    # Create a temporary test image
    test_image_path = tmp_path / "test.png"
    cv2.imwrite(str(test_image_path), np.ones((100, 100, 3), dtype=np.uint8))

    result = load_image(str(test_image_path))
    assert result.shape == (100, 100, 3)

    # Test invalid path
    with pytest.raises(ValueError):
        load_image("nonexistent.png")


def test_get_image_dimensions(sample_image):
    width, height = get_image_dimensions(sample_image)
    assert width == 100
    assert height == 100


def test_get_image_contours(sample_image):
    contours = get_image_contours(sample_image)
    assert contours is not None


def test_process_image(tmp_path):
    # Create a temporary test image
    test_image_path = tmp_path / "test.png"
    cv2.imwrite(str(test_image_path), np.ones((100, 100, 3), dtype=np.uint8))

    image, lines = process_image(str(test_image_path))
    assert image.shape == (100, 100, 3)
    assert lines is not None

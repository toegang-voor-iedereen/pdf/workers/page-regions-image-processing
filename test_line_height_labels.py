import pytest
import numpy as np
from line_height_labels import get_line_height_labels


@pytest.fixture
def sample_arrays():
    return {
        "most_common": np.array([10.0, 12.0, 14.0]),
        "largest": np.array([14.0, 16.0, 18.0]),
        "least_common": np.array([8.0, 16.0]),
    }


def test_empty_result():
    result = get_line_height_labels(0, 1, np.array([]), np.array([]), np.array([]))
    assert len(result) == 0


def test_most_common_line_height(sample_arrays):
    result = get_line_height_labels(
        12.0,
        2,
        sample_arrays["most_common"],
        sample_arrays["largest"],
        sample_arrays["least_common"],
    )
    assert any(label["name"] == "most_common_line_height" for label in result)


def test_least_common_line_height(sample_arrays):
    result = get_line_height_labels(
        8.0,
        2,
        sample_arrays["most_common"],
        sample_arrays["largest"],
        sample_arrays["least_common"],
    )
    assert any(label["name"] == "least_common_line_height" for label in result)


def test_largest_line_height(sample_arrays):
    result = get_line_height_labels(
        18.0,
        1,
        sample_arrays["most_common"],
        sample_arrays["largest"],
        sample_arrays["least_common"],
    )
    assert any(label["name"] == "largest_line_height" for label in result)


def test_multiple_labels(sample_arrays):
    # Test value present in both largest and least_common arrays
    result = get_line_height_labels(
        16.0,
        2,
        sample_arrays["most_common"],
        sample_arrays["largest"],
        sample_arrays["least_common"],
    )
    assert len(result) == 2
    labels = {label["name"] for label in result}
    assert "largest_line_height" in labels
    assert "least_common_line_height" in labels


def test_no_groups():
    result = get_line_height_labels(
        10.0, 1, np.array([10.0]), np.array([10.0]), np.array([10.0])
    )
    assert len(result) == 1
    assert result[0]["name"] == "largest_line_height"


def test_confidence_values(sample_arrays):
    result = get_line_height_labels(
        12.0,
        2,
        sample_arrays["most_common"],
        sample_arrays["largest"],
        sample_arrays["least_common"],
    )
    assert all(label["confidence"] == 1.0 for label in result)

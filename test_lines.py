import cv2
import pytest
import numpy as np
from lines import (
    detect_hough_lines,
    filter_hough_lines,
    get_intersections,
    get_line_angle,
    group_intersections,
    intersection,
)


# Intersection


def test_intersection_intersecting_lines():
    """
    Two lines intersecting at the origin
    """
    line1 = (0, np.pi / 4)  # 45 degrees
    line2 = (0, -np.pi / 4)  # -45 degrees
    expected = (0, 0)
    assert (
        intersection(line1, line2) == expected
    ), "Lines should intersect at the origin"


def test_intersection_parallel_lines():
    """
    Parallel lines do not intersect
    """
    line1 = (1, 0)  # Horizontal line 1 unit away from origin
    line2 = (2, 0)  # Another horizontal line, 2 units away from origin
    assert intersection(line1, line2) is None, "Parallel lines should not intersect"


def test_intersection_identical_lines():
    """
    Identical lines could be considered as intersecting everywhere or no single intersection point
    """
    line1 = (1, np.pi / 4)
    line2 = (1, np.pi / 4)
    # Assuming the implementation returns None for identical lines
    assert (
        intersection(line1, line2) is None
    ), "Identical lines handling depends on implementation"


def test_intersection_perpendicular_lines():
    """
    Lines that are perpendicular should intersect
    """
    line1 = (1, 0)  # Horizontal line
    line2 = (1, np.pi / 2)  # Vertical line
    # Assuming (1,1) is the intersection point
    assert intersection(line1, line2) == (1, 1), "Perpendicular lines should intersect"


# Get line angle


def test_get_line_angle_horizontal_line():
    """
    Horizontal line: theta = 0 radians
    """
    line = (0, 0)
    expected_angle = 0
    assert (
        get_line_angle(line) == expected_angle
    ), "Expected horizontal line to have 0 degree angle"


def test_get_line_angle_vertical_line():
    """
    Vertical line: theta = pi/2 radians
    """
    line = (0, np.pi / 2)
    expected_angle = 90
    assert (
        get_line_angle(line) == expected_angle
    ), "Expected vertical line to have 90 degree angle"


def test_get_line_angle_45_degree_line():
    """
    45-degree line: theta = pi/4 radians
    """
    line = (0, np.pi / 4)
    expected_angle = 45
    angle = get_line_angle(line)
    assert np.isclose(
        angle, expected_angle, atol=0.01
    ), "Expected 45-degree line to be close to 45 degrees"


def test_get_line_angle_135_degree_line():
    """
    135-degree line: theta = 3*pi/4 radians
    """
    line = (0, 3 * np.pi / 4)
    expected_angle = 135
    angle = get_line_angle(line)
    assert np.isclose(
        angle, expected_angle, atol=0.01
    ), "Expected 135-degree line to be close to 135 degrees"


def test_get_line_angle_accuracy():
    """
    Test the function's accuracy for a known angle
    """
    # Let's test for an angle of 60 degrees: theta = pi/3 radians
    line = (0, np.pi / 3)
    expected_angle = 60
    angle = get_line_angle(line)
    assert np.isclose(
        angle, expected_angle, atol=0.01
    ), "Expected angle to be close to 60 degrees"


# get_intersections


@pytest.fixture(name="basic_lines")
def fixture_basic_lines():
    """
    A vertical and a horizontal line in Hough space representation
    """
    return np.array([[[10, 0]], [[10, np.pi / 2]]])


@pytest.fixture(name="basic_interpreted_content")
def fixture_basic_interpreted_content():
    """
    Simulated interpreted content with a single bounding box
    """
    return [
        {
            "bbox": {"top": 0.1, "left": 0.1, "right": 0.5, "bottom": 0.5},
            "classification": "example",
            "confidence": 0.9,
            "attributes": {},
            "labels": None,
            "children": None,
        }
    ]


def test_get_intersections_basic_functionality(basic_lines):
    """
    Basic intersections without interpreted content
    """
    image_width, image_height = 100, 100
    interpreted_content = []
    expected_intersections = [(0, 0), (100, 0), (0, 100), (0, 10), (10, 0)]
    actual_intersections = get_intersections(
        interpreted_content, basic_lines, image_width, image_height
    )
    assert sorted(actual_intersections) == sorted(
        expected_intersections
    ), "Should calculate correct intersections for basic lines"


def test_get_intersections_with_interpreted_content(
    basic_lines, basic_interpreted_content, image_width=100, image_height=100
):
    """
    Intersections with interpreted content
    """
    top_intersection_y = int(basic_interpreted_content[0]["bbox"]["top"] * image_height)
    bottom_intersection_y = int(
        basic_interpreted_content[0]["bbox"]["bottom"] * image_height
    )
    # Expect intersections from the basic lines and the bounding box of the interpreted content
    expected_intersections = [
        (0, 0),
        (100, 0),
        (0, 100),
        (0, top_intersection_y),
        (0, bottom_intersection_y),
        (0, 10),
        (10, 0),
    ]
    actual_intersections = get_intersections(
        basic_interpreted_content, basic_lines, image_width, image_height
    )
    assert sorted(actual_intersections) == sorted(
        expected_intersections
    ), "Should include intersections from interpreted content"


def test_get_intersections_no_lines():
    """
    Scenario without any lines
    """
    image_width, image_height = 100, 100
    interpreted_content = []
    lines = np.array([])  # No lines detected
    expected_intersections = [
        (0, 0),
        (100, 0),
        (0, 100),
    ]  # Default intersections without any lines
    actual_intersections = get_intersections(
        interpreted_content, lines, image_width, image_height
    )
    assert sorted(actual_intersections) == sorted(
        expected_intersections
    ), "Should handle case with no lines"


# group_intersections


def test_group_intersections_basic_clustering():
    """
    Points that should be clustered together
    """
    intersections = [(1, 1), (2, 2), (100, 100), (101, 101)]
    # Expecting integer-rounded centroids of the clusters
    expected_groups = [(1, 1), (100, 100)]  # Centroids, then rounded
    assert sorted(group_intersections(intersections)) == sorted(
        expected_groups
    ), "Should correctly group and calculate centroids of intersections"


def test_group_intersections_single_point():
    """
    A single point should be its own cluster
    """
    intersections: list[tuple[int, int]] = [(50, 50)]
    expected_groups = [(50, 50)]  # Single point, no change expected
    assert (
        group_intersections(intersections) == expected_groups
    ), "Single points should be returned as their own group"


def test_group_intersections_distinct_points():
    """
    Points far apart should each be their own cluster
    """
    intersections = [(0, 0), (100, 100), (200, 200)]
    expected_groups = [(0, 0), (100, 100), (200, 200)]  # Each as its own cluster
    assert sorted(group_intersections(intersections)) == sorted(
        expected_groups
    ), "Distinct points should not be grouped together"


def test_group_intersections_mixed_distances():
    """
    A mix of close and distant points
    """
    intersections = [(1, 1), (2, 2), (100, 100), (200, 200), (201, 201)]
    expected_groups = [
        (1, 1),
        (100, 100),
        (200, 200),
    ]  # Close points grouped, centroids calculated, then rounded
    assert sorted(group_intersections(intersections)) == sorted(
        expected_groups
    ), "Should group only nearby points and calculate their centroids correctly"


# detect_hough_lines

# Define the default lines to be returned if no lines are detected
horizontal_line = [0, 0]
vertical_line = [0, np.pi / 2]


def create_blank_image(width=100, height=100):
    """
    Create a blank (black) image.
    """
    return np.zeros((height, width), dtype=np.uint8)


def create_image_with_line(width=100, height=100, start=(10, 10), end=(90, 90)):
    """
    Create an image with a single white line.
    """
    image = create_blank_image(width, height)
    cv2.line(image, start, end, (255, 255, 255), 2)  # Draw a white line
    return image


def test_detect_hough_lines_detect_no_lines():
    """
    Test detection on a blank image should return default lines.
    """
    blank_image = create_blank_image()
    lines = detect_hough_lines(blank_image)
    # Assuming the default lines are the only ones returned
    assert np.array_equal(
        lines, np.array([[[0, 0]], [[0, np.pi / 2]]], dtype=np.float32)
    ), "Should return default horizontal and vertical lines for a blank image"


def test_detect_hough_lines_detect_horizontal_line():
    """
    Test detection of a single horizontal line.
    """
    image = create_image_with_line(start=(10, 50), end=(90, 50))
    lines = detect_hough_lines(image)
    # Verify that detected lines include more than just the default lines
    assert lines is not None and not np.array_equal(
        lines, np.array([horizontal_line, vertical_line])
    ), "Should detect the horizontal line in addition to default lines"


def test_detect_hough_lines_detect_vertical_line():
    """
    Test detection of a single vertical line.
    """
    image = create_image_with_line(start=(50, 10), end=(50, 90))
    lines = detect_hough_lines(image)
    # Verify that detected lines include more than just the default lines
    assert lines is not None and not np.array_equal(
        lines, np.array([horizontal_line, vertical_line])
    ), "Should detect the vertical line in addition to default lines"


# filter_hough_lines


def test_empty_filter_hough_lines():
    """
    Test case for when the input lines array is empty.
    """
    empty_lines = np.empty(
        (0, 1, 2)
    )  # Shape (0, 1, 2) to simulate an empty array with the same structure

    result = filter_hough_lines(empty_lines)

    # Check that the result is also an empty array with the same shape
    assert (
        result.shape == empty_lines.shape
    ), f"Expected shape {empty_lines.shape}, got {result.shape}"
    assert result.size == 0, f"Expected result size to be 0, got {result.size}"


def test_filter_hough_lines_all_allowed_angles():
    """
    All lines are at an allowed angle
    """
    # Assuming the function filters to retain only horizontal (0 degrees) and vertical (90 degrees) lines
    lines = np.array(
        [[[0, np.deg2rad(0)]], [[0, np.deg2rad(90)]]]  # Horizontal  # Vertical
    )
    filtered_lines = filter_hough_lines(lines)
    assert len(filtered_lines) == 2, "Should retain all lines at allowed angles"


def test_filter_hough_lines_lines_at_disallowed_angles():
    """
    All lines are at disallowed angles
    """
    # Example disallowed angles: 45 degrees and 135 degrees
    lines = np.array([[[0, np.deg2rad(45)]], [[0, np.deg2rad(135)]]])
    filtered_lines = filter_hough_lines(lines)
    assert len(filtered_lines) == 0, "Should filter out all lines at disallowed angles"


def test_filter_hough_lines_mixed_angles():
    """
    A mix of allowed and disallowed angles
    """
    lines = np.array(
        [
            [[0, np.deg2rad(0)]],  # Allowed
            [[0, np.deg2rad(45)]],  # Disallowed
            [[0, np.deg2rad(90)]],  # Allowed
        ]
    )
    filtered_lines = filter_hough_lines(lines)
    assert (
        len(filtered_lines) == 2
    ), "Should correctly filter mixed angles, retaining only allowed angles"


def test_filter_hough_lines_edge_case_angles():
    """
    A mix of allowed and disallowed angles that are very near to the threshold
    """
    # Assuming a small tolerance around allowed angles, e.g., +/- 0.01 degree around 0 and 90 degrees
    lines = np.array(
        [
            [[0, np.deg2rad(0.009)]],  # Close to allowed angle, should be retained
            [[0, np.deg2rad(89.999)]],  # Close to allowed angle, should be retained
            [[0, np.deg2rad(90.02)]],  # Just outside tolerance, should be filtered
        ]
    )
    filtered_lines = filter_hough_lines(lines)
    assert (
        len(filtered_lines) == 2
    ), "Should retain lines close to allowed angles and filter those just outside"

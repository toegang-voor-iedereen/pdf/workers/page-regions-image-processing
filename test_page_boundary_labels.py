import pytest
from shapely import Polygon
from page_boundary_labels import get_page_boundary_labels


@pytest.fixture
def create_polygon():
    def _create_polygon(top, left, right, bottom):
        return Polygon([(left, top), (right, top), (right, bottom), (left, bottom)])

    return _create_polygon


def test_no_boundary_labels(create_polygon):
    center_polygon = create_polygon(0.4, 0.1, 0.9, 0.6)
    result = get_page_boundary_labels(center_polygon)
    assert len(result) == 0


def test_footer_label(create_polygon):
    footer_polygon = create_polygon(0.92, 0.1, 0.9, 0.98)
    result = get_page_boundary_labels(footer_polygon)
    assert len(result) == 1
    assert result[0]["name"] == "footer"
    assert result[0]["confidence"] == 0.75


def test_header_label(create_polygon):
    header_polygon = create_polygon(0.02, 0.1, 0.9, 0.08)
    result = get_page_boundary_labels(header_polygon)
    assert len(result) == 1
    assert result[0]["name"] == "header"
    assert result[0]["confidence"] == 0.75


def test_partial_intersection(create_polygon):
    partial_polygon = create_polygon(0.85, 0.1, 0.9, 0.95)
    result = get_page_boundary_labels(partial_polygon)
    assert len(result) == 0


def test_both_regions(create_polygon):
    full_page_polygon = create_polygon(0, 0, 1, 1)
    result = get_page_boundary_labels(full_page_polygon)
    assert len(result) == 0

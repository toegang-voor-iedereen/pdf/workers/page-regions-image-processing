"""
Utility module for line detection and intersection calculation.

This module provides functions for detecting lines in images using the Hough transform,
calculating intersections of these lines with the image boundaries and sidelines, and
performing various operations related to line geometry.
"""

import cv2
import scipy.cluster.hierarchy as hcluster
import numpy as np

from kimiworker import ContentDefinition


def intersection(line1: tuple[float, float], line2: tuple[float, float]):
    """
    Finds the intersection of two lines given in Hesse normal form.

    Returns closest integer pixel locations.

    Args:
        line1 (tuple[float, float]): Parameters of the first line (rho, theta).
        line2 (tuple[float, float]): Parameters of the second line (rho, theta).

    Returns:
        tuple[int, int]: Closest integer pixel coordinates of the intersection point.
    """
    rho1, theta1 = line1
    rho2, theta2 = line2

    # Construct the coefficient matrix for solving simultaneous equations
    a = np.array([[np.cos(theta1), np.sin(theta1)], [np.cos(theta2), np.sin(theta2)]])

    # Construct the constant vector
    b = np.array([[rho1], [rho2]])

    try:
        # Solve the system of equations to find the intersection point
        intersection_point = np.linalg.solve(a, b)
    except np.linalg.LinAlgError:
        # If the coefficient matrix is singular, lines are parallel and do not intersect
        return None

    # Round the intersection coordinates to the nearest integers
    x0, y0 = int(np.round(intersection_point[0, 0])), int(
        np.round(intersection_point[1, 0])
    )

    return (x0, y0)


def get_line_angle(line: tuple[float, float]) -> float:
    """
    Get the angle (in degrees) of a line.

    Args:
        line (tuple[float, float]): Parameters of the line (rho, theta).

    Returns:
        float: Angle of the line in degrees, normalized to [0, 180) for undirected lines.
    """
    _, theta = line

    # Theta is the angle between the line's normal and the x-axis.
    # For the line's direction angle, we adjust theta accordingly.
    angle_deg = np.degrees(theta)

    # Since we're interested in the line's direction, not the normal's,
    # we adjust for the common case where the line's angle is effectively
    # the complement to theta's angle with the x-axis.

    # Normalize angle to [0, 180) for undirected lines
    normalized_angle = angle_deg % 180

    return normalized_angle


def get_intersections(
    interpreted_content: list[ContentDefinition],
    lines: np.ndarray,
    image_width: int,
    image_height: int,
) -> list[tuple[int, int]]:
    """
    Get line intersection points at document edges and text boundaries.

    Calculates intersection points between:
    - Document corners and edges
    - Content box boundaries along left edge
    - Detected lines with horizontal/vertical edges

    Args:
        interpreted_content: OCR text content with bounding boxes
        lines: Detected Hough lines as (rho, theta) pairs
        image_width: Width of document in pixels
        image_height: Height of document in pixels

    Returns:
        List of (x,y) intersection point coordinates
    """
    # Start with corners and edges
    intersections = [(0, 0), (image_width, 0), (0, image_height)]

    # Add content box intersections with left edge
    intersections.extend(
        (0, int(c["bbox"]["top"] * image_height)) for c in interpreted_content
    )
    intersections.extend(
        (0, int(c["bbox"]["bottom"] * image_height)) for c in interpreted_content
    )

    if lines.size > 0:
        # For each detected line, find intersections with horizontal and vertical edges
        for rho, theta in lines[:, 0]:
            angle = np.degrees(theta) % 180  # Normalize angle to 0-180 range

            # Check against horizontal (0°) and vertical (90°) edges
            for s_angle in (0, 90):
                # Look for nearly perpendicular lines (90° ±1°)
                if 89 < abs(s_angle - angle) < 91:
                    # Calculate intersection point
                    if point := intersection((0, s_angle * np.pi / 180), (rho, theta)):
                        x, y = point
                        # Only keep points inside image bounds
                        if 0 <= x <= image_width and 0 <= y <= image_height:
                            intersections.append(point)

    return intersections


def group_intersections(intersections: list[tuple[int, int]]) -> list[tuple[int, int]]:
    """
    Group intersections using hierarchical clustering.

    Args:
        intersections (list[tuple[int, int]]): List of intersections.

    Returns:
        list[tuple[int, int]]: Grouped intersections.
    """
    if len(intersections) < 2:
        return intersections
    points = np.array(intersections)
    clusters = hcluster.fclusterdata(points, 10, criterion="distance")
    return [
        (int(x), int(y))
        for x, y in [
            np.mean(points[clusters == i], axis=0) for i in range(1, max(clusters) + 1)
        ]
    ]


def detect_hough_lines(image: np.ndarray) -> np.ndarray:
    """
    Detects Hough Lines in the given image.

    Args:
        image (np.ndarray): The input image.

    Returns:
        np.ndarray: Detected Hough lines.
    """
    lines = cv2.HoughLines(image, rho=1, theta=np.pi / 180, threshold=50)
    return (
        lines
        if lines is not None
        else np.array([[[0, 0]], [[0, np.pi / 2]]], dtype=np.float32)
    )


def filter_hough_lines(lines: np.ndarray) -> np.ndarray:
    """
    Filters Hough lines based on angle.

    Args:
        lines (np.ndarray): Array of Hough lines.

    Returns:
        np.ndarray: Filtered Hough lines.
    """
    if lines.size == 0:
        return np.empty((0, *lines.shape[1:]))
    angles = np.degrees(lines[:, 0, 1])
    mask = np.min(np.abs(angles[:, None] - [0, 90, 180, 270]), axis=1) <= 0.01
    return lines[mask]

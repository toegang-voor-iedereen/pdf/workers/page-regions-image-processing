import pytest
import numpy as np
from unittest.mock import Mock, patch, MagicMock
from shapely import STRtree
from main import (
    get_content_result,
    generate_labels,
    get_job_handler,
    COLOR_TOLERANCE,
)
from kimiworker import BoundingBox, WorkerJob, ContentDefinition


@pytest.fixture
def mock_image():
    return np.zeros((100, 100, 3), dtype=np.uint8)


@pytest.fixture
def mock_interpreted_content() -> list[ContentDefinition]:
    return [
        {
            "classification": "interpretedContent",
            "labels": [],
            "bbox": {"top": 0.1, "left": 0.1, "right": 0.3, "bottom": 0.2},
            "confidence": 0.9,
            "attributes": {"text": "Sample text", "lineHeight": 12.0},
            "children": [],
        }
    ]


@pytest.fixture
def mock_grouped_intersections():
    return [(10, 10), (20, 20), (30, 30)]


@pytest.fixture
def mock_line_height_data():
    return (2, [12.0], [14.0], [10.0])


def test_generate_labels(mock_image, mock_interpreted_content, mock_line_height_data):
    bbox: BoundingBox = {"top": 0.1, "left": 0.1, "right": 0.3, "bottom": 0.2}
    text_boxes_tree = STRtree([])
    bg_color = np.array([255, 255, 255])

    with patch("main.get_top_colors") as mock_top_colors, patch(
        "main.get_highest_positioned_content"
    ) as mock_highest:

        mock_top_colors.return_value = (
            [np.array([240, 240, 240]), np.array([0, 0, 0])],
            [0.8, 0.2],
        )
        mock_highest.return_value = mock_interpreted_content[0]

        labels, attributes = generate_labels(
            {0},
            mock_interpreted_content,
            bbox,
            mock_image,
            text_boxes_tree,
            bg_color,
            mock_line_height_data,
        )

        assert isinstance(labels, list)
        assert isinstance(attributes, dict)
        assert any(label["name"] == "text" for label in labels)


def test_generate_labels_no_overlapping_indices(mock_image, mock_line_height_data):
    bbox: BoundingBox = {"top": 0.1, "left": 0.1, "right": 0.3, "bottom": 0.2}
    text_boxes_tree = STRtree([])
    bg_color = np.array([255, 255, 255])

    with pytest.raises(ValueError, match="No overlapping content found"):
        generate_labels(
            set(),  # Empty set of overlapping indices
            [],  # Empty content list
            bbox,
            mock_image,
            text_boxes_tree,
            bg_color,
            mock_line_height_data,
        )


def test_generate_labels_non_black_text(
    mock_image, mock_interpreted_content, mock_line_height_data
):
    bbox: BoundingBox = {"top": 0.1, "left": 0.1, "right": 0.3, "bottom": 0.2}
    text_boxes_tree = STRtree([])
    bg_color = np.array([255, 255, 255])  # White background

    with patch("main.get_top_colors") as mock_top_colors, patch(
        "main.get_highest_positioned_content"
    ) as mock_highest, patch("main.is_black_strict") as mock_is_black:

        # Set up white background color and blue text color
        mock_top_colors.return_value = (
            [np.array([255, 255, 255]), np.array([0, 0, 255])],  # White bg, blue text
            [0.8, 0.2],
        )
        mock_highest.return_value = mock_interpreted_content[0]
        mock_is_black.return_value = False  # Text color is not black

        labels, attributes = generate_labels(
            {0},
            mock_interpreted_content,
            bbox,
            mock_image,
            text_boxes_tree,
            bg_color,
            mock_line_height_data,
        )

        # Verify the text_color_not_black label was added
        assert any(
            label["name"] == "text_color_not_black" and label["confidence"] == 1.0
            for label in labels
        )
        # Verify the textColor attribute was set to the hex value of blue
        assert attributes["textColor"] == "#0000ff"


def test_generate_labels_no_content_for_indices(mock_image, mock_line_height_data):
    bbox: BoundingBox = {"top": 0.1, "left": 0.1, "right": 0.3, "bottom": 0.2}
    text_boxes_tree = STRtree([])
    bg_color = np.array([255, 255, 255])

    with pytest.raises(ValueError, match="Overlapping indices exceed content length"):
        generate_labels(
            {0, 1},  # Indices that don't exist in the content list
            [],  # Empty content list, so indices won't map to anything
            bbox,
            mock_image,
            text_boxes_tree,
            bg_color,
            mock_line_height_data,
        )



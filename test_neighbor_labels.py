import pytest
from shapely import STRtree
from shapely.geometry import box as shapely_box
from neighbor_labels import get_neighbor_labels
from kimiworker import BoundingBox


@pytest.fixture
def sample_tree():
    boxes = [
        shapely_box(0, 0, 1, 1),  # base box
        shapely_box(0, 1.001, 1, 2),  # top neighbor
        shapely_box(0, -1, 1, -0.001),  # bottom neighbor
    ]
    return STRtree(boxes)


def test_no_neighbors():
    isolated_box: BoundingBox = {"left": 5, "right": 6, "top": 5, "bottom": 4}
    tree = STRtree([shapely_box(0, 0, 1, 1)])
    result = get_neighbor_labels(tree, isolated_box)
    assert len(result) == 0


def test_top_neighbor(sample_tree):
    bbox: BoundingBox = {"left": 0, "right": 1, "top": 1, "bottom": 0}
    result = get_neighbor_labels(sample_tree, bbox)
    assert any(label["name"] == "has_neighbor_top" for label in result)


def test_bottom_neighbor(sample_tree):
    bbox: BoundingBox = {"left": 0, "right": 1, "top": 1, "bottom": 0}
    result = get_neighbor_labels(sample_tree, bbox)
    assert any(label["name"] == "has_neighbor_bottom" for label in result)


def test_both_neighbors(sample_tree):
    bbox: BoundingBox = {"left": 0, "right": 1, "top": 1, "bottom": 0}
    result = get_neighbor_labels(sample_tree, bbox)
    assert len(result) == 2
    labels = {label["name"] for label in result}
    assert "has_neighbor_top" in labels
    assert "has_neighbor_bottom" in labels


def test_confidence_values(sample_tree):
    bbox: BoundingBox = {"left": 0, "right": 1, "top": 1, "bottom": 0}
    result = get_neighbor_labels(sample_tree, bbox)
    assert all(label["confidence"] == 1.0 for label in result)


def test_outside_threshold():
    boxes = [
        shapely_box(0, 1.01, 1, 2),  # too far above
        shapely_box(0, -2, 1, -0.01),  # too far below
    ]
    tree = STRtree(boxes)
    bbox: BoundingBox = {"left": 0, "right": 1, "top": 1, "bottom": 0}
    result = get_neighbor_labels(tree, bbox)
    assert len(result) == 0

"""
This module provides functions for validating text attributes.
"""

from math import isnan
import re
from kimiworker import ContentAttribute


def in_range(value: float, max_value: float) -> bool:
    """
    Check if a value is within the range of 0 to max_value.

    Args:
        value (float): The value to be checked.
        max_value (float): The maximum allowed value.

    Returns:
        bool: True if the value is within the range, False otherwise.
    """
    # Check if the value is within the range of 0 to max_value
    return 0 <= value <= max_value


def is_numeric(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is numeric, regardless of whether it is a string or a number.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is numeric, False otherwise.
    """
    try:
        # Check if the content attribute is an instance of string, int, or float
        assert isinstance(text, (str, int, float))
        # Attempt to convert the content attribute to a float
        val = float(text)
        return not isnan(val)
    except (ValueError, AssertionError):
        return False


def starts_numeric(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that starts with a numeric character.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string starting with a numeric character, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Attempt to convert the first character of the string to a float
        float(text[0])
        return True
    except (ValueError, AssertionError, IndexError):
        return False


def starts_with_capital(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that starts with a capital letter.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string starting with a capital letter, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Check if the string starts with a numeric character
        assert not starts_numeric(text)
        # Check if the first character of the string is uppercase
        return text[0].upper() == text[0]
    except (ValueError, AssertionError, IndexError):
        return False


def starts_with_single_letter(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that starts with a single letter, that could be followed by a dot.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string starting with a single letter, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Check if the string starts with a numeric character
        assert not starts_numeric(text)

        first_part_without_dot = text.split(" ")[0].replace(".", "")

        assert first_part_without_dot.isalpha()

        return len(first_part_without_dot) == 1
    except (ValueError, AssertionError, IndexError):
        return False


def starts_with_roman_numeral(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that starts with a roman numeral followed by a dot.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string starting with a roman numeral followed by a dot, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Check if the string starts with a numeric character
        assert not starts_numeric(text)

        first_part = text.strip().split(" ")[0]
        first_part_without_dot = first_part.replace(".", "")

        assert len(first_part) != len(first_part_without_dot)

        assert first_part_without_dot.isalpha()

        return bool(
            re.search(
                r"^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$",
                first_part_without_dot,
            )
        )
    except (ValueError, AssertionError, IndexError):
        return False


def is_single_word(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that consists of a single word.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string consisting of a single word, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Split the string by whitespace and check if there is only one element
        return len(text.split()) == 1
    except (ValueError, AssertionError):
        return False


def is_uppercase(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that is fully uppercase.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string that is fully uppercase, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Check if the string is not numeric
        assert not is_numeric(text)
        # Check if the string is fully uppercase
        return text.upper() == text
    except (ValueError, AssertionError):
        return False


def is_lowercase(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that is fully lowercase.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string that is fully lowercase, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Check if the string is not numeric
        assert not is_numeric(text)
        # Check if the string is fully lowercase
        return text.lower() == text
    except (ValueError, AssertionError):
        return False


def ends_with_colon(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that ends with a colon.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string that ends with a colon, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Check if the stripped string ends with a colon
        return text.strip().endswith(":")
    except (ValueError, AssertionError):
        return False


def ends_with_semicolon(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that ends with a semicolon.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string that ends with a semicolon, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Check if the stripped string ends with a semicolon
        return text.strip().endswith(";")
    except (ValueError, AssertionError):
        return False


def ends_with_period(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that ends with a period.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string that ends with a period, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Check if the stripped string ends with a period
        return text.strip().endswith(".")
    except (ValueError, AssertionError):
        return False


def has_list_item_structure(text: ContentAttribute | None) -> bool:
    """
    Determine if a content attribute is a string that has a list-item-like structure.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string that has a list-item-like structure, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)

        # Get the first part of the string, which is the part before the first space
        first_part = text.split(" ")[0].strip()

        first_part_is_numeric = is_numeric(first_part.replace(".", ""))

        max_length = 3 if first_part_is_numeric else 2

        # Check if the string starts with a bullet point or looks like a list item
        # (where 1., 13. and a. are valid, but aa. is not)
        return first_part[0] in ["*", "-", "•"] or (
            len(first_part) <= max_length and first_part.endswith(".")
        )

    except (ValueError, AssertionError):
        return False


def contains_year(text: ContentAttribute | None) -> bool:
    """
    Determine if the content attribute is a string that contains a year between 1800 and 2100.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string that contains a year between 1800 and 2100, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Search for a year between 1800 and 2100
        return bool(re.search(r"\b(?:18|19|20|21)\d{2}\b", text))
    except (ValueError, AssertionError):
        return False


def ends_with_year(text: ContentAttribute | None) -> bool:
    """
    Determine if the content attribute is a string that ends with a year between 1800 and 2100.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string that ends with a year between 1800 and 2100, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Search for a year between 1800 and 2100 at the end of the string
        return bool(re.search(r'\b(?:18|19|20|21)\d{2}\b[!.;:?")]*$', text.strip()))
    except (ValueError, AssertionError):
        return False


def contains_month(text: ContentAttribute | None) -> bool:
    """
    Determine if the content attribute is a string that contains a written month.

    Args:
        text (ContentAttribute | None): The content attribute to be checked.

    Returns:
        bool: True if the content attribute is a string that contains a written month, False otherwise.
    """
    try:
        # Check if the content attribute is a string
        assert isinstance(text, str)
        # Define a list of written month names
        months = [
            "januari",
            "january",
            "februari",
            "february",
            "maart",
            "march",
            "april",
            "avril",
            "mei",
            "may",
            "juni",
            "june",
            "juli",
            "july",
            "julij",
            "augustus",
            "august",
            "september",
            "septembre",
            "oktober",
            "october",
            "octobre",
            "november",
            "novembre",
            "december",
            "decembre",
        ]
        # Check if any of the month names are present in the lowercase version of the text
        return any(month in text.lower() for month in months)
    except (ValueError, AssertionError):
        return False

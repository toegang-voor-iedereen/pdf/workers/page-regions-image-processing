"""
Module for converting bounding boxes.

This module provides functions for converting bounding boxes between different formats.
It includes functions to convert pixel-based bounding boxes to percentage-based bounding boxes,
and to convert bounding boxes to Shapely Polygon objects.
"""

from functools import lru_cache
from shapely.geometry import Polygon
from kimiworker import BoundingBox


@lru_cache
def convert_bounding_box(
    left: float,
    top: float,
    right: float,
    bottom: float,
    image_width: int,
    image_height: int,
) -> BoundingBox:
    """
    Convert pixel-based bounding box coordinates to percentage-based bounding box.

    Args:
        left (float): The left coordinate of the bounding box.
        top (float): The top coordinate of the bounding box.
        right (float): The right coordinate of the bounding box.
        bottom (float): The bottom coordinate of the bounding box.
        image_width (int): The width of the image.
        image_height (int): The height of the image.

    Returns:
        BoundingBox: A dictionary representing the converted bounding box with
                     percentage-based coordinates.
    """
    # Calculate percentage-based coordinates for the bounding box
    return {
        "top": top / image_height,
        "left": left / image_width,
        "bottom": bottom / image_height,
        "right": right / image_width,
    }


def bounding_box_to_polygon(bbox: BoundingBox) -> Polygon:
    """
    Convert a BoundingBox object to a Shapely Polygon.

    Args:
        bbox (BoundingBox): A dictionary representing the bounding box with
                            percentage-based coordinates.

    Returns:
        Polygon: A Shapely Polygon representing the bounding box.
    """
    # Create a Shapely Polygon from the bounding box coordinates
    return Polygon.from_bounds(
        bbox.get("left"), bbox.get("top"), bbox.get("right"), bbox.get("bottom")
    )


def get_intersection_ratio(bbox1: BoundingBox, bbox2: BoundingBox) -> float:
    """
    Calculate intersection over area ratio between two bounding boxes.
    Uses the smaller area of the two boxes as denominator for ratio calculation.

    Args:
        bbox1: First bounding box with top, left, right, bottom coordinates (0-1)
        bbox2: Second bounding box with top, left, right, bottom coordinates (0-1)

    Returns:
        float: Intersection area divided by smaller box area. 0 if no intersection.
    """
    x1 = max(bbox1["left"], bbox2["left"])
    y1 = max(bbox1["top"], bbox2["top"])
    x2 = min(bbox1["right"], bbox2["right"])
    y2 = min(bbox1["bottom"], bbox2["bottom"])

    if x1 >= x2 or y1 >= y2:
        return 0.0

    intersection = (x2 - x1) * (y2 - y1)
    min_area = min(
        (bbox1["right"] - bbox1["left"]) * (bbox1["bottom"] - bbox1["top"]),
        (bbox2["right"] - bbox2["left"]) * (bbox2["bottom"] - bbox2["top"]),
    )
    return intersection / min_area

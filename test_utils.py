from kimiworker import WorkerJob, ContentDefinition
import pytest
from utils import (
    extract_best_attribute_value,
    calculate_mean_confidence,
    get_line_height_group,
    get_background_coverage,
    filter_content_result,
)
from unittest.mock import patch
from typing import List


def test_extract_best_attribute_value_with_matching_job_id():
    """
    Tests extract_best_attribute_value function to ensure it correctly finds and returns
    the dictionary with a matching job ID.
    """

    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "123",
                "values": [
                    {"jobId": "122", "value": "A"},
                    {"jobId": "123", "value": "B"},
                    {"jobId": "124", "value": "C"},
                ],
            },
        },
    }

    expected = {"jobId": "123", "value": "B"}
    assert (
        extract_best_attribute_value(job, "interpretedContent") == expected
    ), "Should return the matching job ID dictionary"


def test_extract_best_attribute_value_no_matching_job_id():
    """
    Tests extract_best_attribute_value to verify that it returns None when no job ID matches.
    """
    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "125",
                "values": [
                    {"jobId": "122", "value": "A"},
                    {"jobId": "123", "value": "B"},
                    {"jobId": "124", "value": "C"},
                ],
            },
        },
    }
    assert (
        extract_best_attribute_value(job, "interpretedContent") is None
    ), "Should return None for no matching job ID"


def test_extract_best_attribute_value_empty_values():
    """
    Tests extract_best_attribute_value to ensure it handles empty values list correctly.
    """
    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "125",
                "values": [],
            },
        },
    }
    assert (
        extract_best_attribute_value(job, "interpretedContent") is None
    ), "Should return None when values list is empty"


def test_extract_best_attribute_value_missing_keys():
    """
    Tests extract_best_attribute_value to ensure it correctly handles missing keys.
    """
    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "values": [],
            },
        },
    }
    assert (
        extract_best_attribute_value(job, "interpretedContent") is None
    ), "Should return None when key is missing"


def test_extract_best_attribute_value_incorrect_type():
    """
    Tests extract_best_attribute_value to ensure it returns None when the jobId in values does not match the expected type.
    """
    job: WorkerJob = {
        "recordId": "x",
        "bucketName": "x",
        "filename": "x",
        "attributes": {
            "interpretedContent": {
                "bestJobId": "123",
                "values": [
                    {
                        "jobId": 123,
                        "value": "Incorrect Type",
                    },  # This should not be matched due to type mismatch
                    {"jobId": "123", "value": "Correct Type"},
                ],
            },
        },
    }
    expected = {"jobId": "123", "value": "Correct Type"}
    assert (
        extract_best_attribute_value(job, "interpretedContent") == expected
    ), "Should return the correct type match"


def test_calculate_mean_confidence_empty_list():
    """
    Test that an empty list returns 0 as the mean confidence
    """
    assert calculate_mean_confidence([]) == 0


def test_calculate_mean_confidence_single_item():
    """
    Test mean confidence calculation with a single content item
    """
    content: List[ContentDefinition] = [{"confidence": 0.95}]  # type: ignore
    assert calculate_mean_confidence(content) == 0.95


def test_calculate_mean_confidence_multiple_items():
    """
    Test mean confidence calculation with multiple content items
    """
    content: List[ContentDefinition] = [
        {"confidence": 0.8},
        {"confidence": 0.9},
        {"confidence": 1.0},
    ]  # type: ignore
    assert calculate_mean_confidence(content) == 0.9


def test_calculate_mean_confidence_missing_confidence():
    """
    Test that a TypeError is raised when confidence key is missing
    """
    content: List[ContentDefinition] = [{"confidence": 0.8}, {}]  # type: ignore
    with pytest.raises(TypeError):
        calculate_mean_confidence(content)


def test_get_line_height_group():
    """
    Test get_line_height_group function to ensure it correctly extracts the line height group.
    """
    content: ContentDefinition = {
        "classification": "",
        "attributes": {},
        "labels": [],
        "children": [],
        "confidence": 0.8,
        "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
    }
    with patch(
        "utils.get_content_definition_float_attribute",
        return_value=1.234,
    ):
        assert get_line_height_group(content) == 1.234, "Should return the mocked value"


def test_get_background_coverage():
    """
    Test get_background_coverage function to ensure it correctly extracts the background coverage.
    """
    content: ContentDefinition = {
        "classification": "",
        "attributes": {},
        "labels": [],
        "children": [],
        "confidence": 0.8,
        "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
    }
    with patch(
        "utils.get_content_definition_float_attribute",
        return_value=5.678,
    ):
        assert (
            get_background_coverage(content) == 5.678
        ), "Should return the mocked value"


def test_filter_content_result():
    """
    Test filter_content_result function to ensure it correctly filters content with labels.
    """
    content_result: List[ContentDefinition] = [
        {
            "classification": "",
            "attributes": {},
            "labels": [],
            "children": [],
            "confidence": 0.8,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
        },
        {
            "classification": "",
            "attributes": {},
            "labels": [{"name": "label", "confidence": 0.9}],
            "children": [],
            "confidence": 0.8,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
        },
    ]
    assert (
        len(filter_content_result(content_result)) == 1
    ), "Should return only content with labels"

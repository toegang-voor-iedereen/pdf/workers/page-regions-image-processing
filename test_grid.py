# points_to_grid_coordinates_by_row


from typing import Generator
from grid import pairwise_grid_iterator, points_to_grid_coordinates_by_row


def test_points_to_grid_coordinates_by_row_generate_full_grid_from_points():
    """
    Test that the function generates all combinations of x and y coordinates from the input points
    and groups these combinations by y-coordinate.
    """
    points = [(10, 5), (20, 10), (30, 15)]
    # Expected to generate grid coordinates for every combination of x and y values
    expected = [
        [(10, 5), (20, 5), (30, 5)],  # All x values combined with y=5
        [(10, 10), (20, 10), (30, 10)],  # All x values combined with y=10
        [(10, 15), (20, 15), (30, 15)],  # All x values combined with y=15
    ]
    assert (
        list(points_to_grid_coordinates_by_row(points)) == expected
    ), "The function should generate a full grid of coordinates grouped by row."


def test_points_to_grid_coordinates_by_row_with_duplicate_y_values():
    """
    Test the function with input points that have duplicate y values to ensure it handles them correctly.
    """
    points = [(5, 5), (10, 5), (5, 10), (10, 10)]
    # Even with duplicate y values, expect a full grid combining each x with each y
    expected = [
        [(5, 5), (10, 5)],  # Combinations with y=5
        [(5, 10), (10, 10)],  # Combinations with y=10
    ]
    assert (
        list(points_to_grid_coordinates_by_row(points)) == expected
    ), "The function should handle duplicate y values correctly, generating all x-y combinations."


def test_points_to_grid_coordinates_by_row_single_point():
    """
    Test the function with a single point to verify it still generates the correct grid.
    """
    points: list[tuple[int, int]] = [(10, 10)]
    expected = [[(10, 10)]]  # Only one point, so only one combination possible
    assert (
        list(points_to_grid_coordinates_by_row(points)) == expected
    ), "The function should correctly handle a single point, generating a grid with just that point."


def test_points_to_grid_coordinates_by_row_empty_input():
    """
    Test the function with an empty input list to ensure it returns an empty generator.
    """
    points = []
    expected = []  # No points, so expecting an empty list of rows
    assert (
        list(points_to_grid_coordinates_by_row(points)) == expected
    ), "The function should return an empty list for empty input."


def test_pairwise_grid_iterator_basic():
    """Test basic grid coordinate iteration"""

    def mock_grid() -> Generator[list[tuple[int, int]], None, None]:
        yield [(0, 0), (10, 0)]
        yield [(0, 10), (10, 10)]

    iterator = pairwise_grid_iterator(mock_grid())
    result = list(iterator)

    assert len(result) == 1
    assert result[0] == (0, 0, 0, 10, 10, [(0, 0), (10, 0)], [(0, 10), (10, 10)])


def test_pairwise_grid_iterator_multiple_points():
    """Test grid with multiple points per row"""

    def mock_grid() -> Generator[list[tuple[int, int]], None, None]:
        yield [(0, 0), (5, 0), (10, 0)]
        yield [(0, 10), (5, 10), (10, 10)]

    iterator = pairwise_grid_iterator(mock_grid())
    result = list(iterator)

    assert len(result) == 2
    assert result[0][1:5] == (0, 0, 5, 10)  # First pair coords
    assert result[1][1:5] == (5, 0, 10, 10)  # Second pair coords


def test_pairwise_grid_iterator_unsorted():
    """Test grid with unsorted input coordinates"""

    def mock_grid() -> Generator[list[tuple[int, int]], None, None]:
        yield [(10, 0), (0, 0), (5, 0)]
        yield [(10, 10), (0, 10), (5, 10)]

    iterator = pairwise_grid_iterator(mock_grid())
    result = list(iterator)

    assert len(result) == 2
    assert result[0][1:5] == (0, 0, 5, 10)  # Verify sorting
    assert result[1][1:5] == (5, 0, 10, 10)


def test_pairwise_grid_iterator_empty():
    """Test handling of empty grid"""

    def mock_grid() -> Generator[list[tuple[int, int]], None, None]:
        if False:
            yield []

    iterator = pairwise_grid_iterator(mock_grid())
    assert list(iterator) == []

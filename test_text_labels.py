from text_labels import get_text_labels


def test_get_text_labels():
    assert [label.get("name") for label in get_text_labels("1. Foo bar baz.")] == [
        "starts_numeric",
        "ends_with_period",
        "list_item_like",
    ]


def test_numeric():
    assert {"name": "is_numeric", "confidence": 1.0} in get_text_labels("123")
    assert {"name": "is_numeric", "confidence": 1.0} not in get_text_labels("abc")


def test_starts_numeric():
    assert {"name": "starts_numeric", "confidence": 1.0} in get_text_labels("1abc")
    assert {"name": "starts_numeric", "confidence": 1.0} not in get_text_labels("abc")


def test_starts_with_capital():
    assert {"name": "starts_with_cap", "confidence": 1.0} in get_text_labels("Test")
    assert {"name": "starts_with_cap", "confidence": 1.0} not in get_text_labels("test")


def test_single_word():
    assert {"name": "single_word", "confidence": 1.0} in get_text_labels("word")
    assert {"name": "single_word", "confidence": 1.0} not in get_text_labels(
        "two words"
    )


def test_uppercase():
    assert {"name": "uppercase", "confidence": 1.0} in get_text_labels("TEST")
    assert {"name": "uppercase", "confidence": 1.0} not in get_text_labels("Test")


def test_lowercase():
    assert {"name": "lowercase", "confidence": 1.0} in get_text_labels("test")
    assert {"name": "lowercase", "confidence": 1.0} not in get_text_labels("Test")


def test_ends_with_colon():
    assert {"name": "ends_with_colon", "confidence": 1.0} in get_text_labels("test:")
    assert {"name": "ends_with_colon", "confidence": 1.0} not in get_text_labels("test")


def test_ends_with_semicolon():
    assert {"name": "ends_with_semicolon", "confidence": 1.0} in get_text_labels(
        "test;"
    )
    assert {"name": "ends_with_semicolon", "confidence": 1.0} not in get_text_labels(
        "test"
    )


def test_ends_with_period():
    assert {"name": "ends_with_period", "confidence": 1.0} in get_text_labels("test.")
    assert {"name": "ends_with_period", "confidence": 1.0} not in get_text_labels(
        "test"
    )


def test_list_item_structure():
    assert {"name": "list_item_like", "confidence": 0.9} in get_text_labels("1. test")
    assert {"name": "list_item_like", "confidence": 0.9} not in get_text_labels("test")


def test_contains_year():
    assert {"name": "contains_year", "confidence": 75} in get_text_labels("test 2024")
    assert {"name": "contains_year", "confidence": 75} not in get_text_labels("test")


def test_ends_with_year():
    assert {"name": "ends_with_year", "confidence": 0.9} in get_text_labels("test 2024")
    assert {"name": "ends_with_year", "confidence": 0.9} not in get_text_labels(
        "2024 test"
    )


def test_contains_month():
    assert {"name": "contains_month", "confidence": 1.0} in get_text_labels(
        "January test"
    )
    assert {"name": "contains_month", "confidence": 1.0} not in get_text_labels("test")


def test_starts_with_roman_numeral():
    assert {"name": "starts_with_roman_numeral", "confidence": 1.0} in get_text_labels(
        "IV. test"
    )
    assert {
        "name": "starts_with_roman_numeral",
        "confidence": 1.0,
    } not in get_text_labels("test")


def test_starts_with_single_letter():
    assert {"name": "starts_with_single_letter", "confidence": 1.0} in get_text_labels(
        "A. test"
    )
    assert {
        "name": "starts_with_single_letter",
        "confidence": 1.0,
    } not in get_text_labels("test")


def test_none_input():
    assert get_text_labels(None) == []


def test_multiple_labels():
    text = "A. Test 2024:"
    labels = get_text_labels(text)
    expected_labels = [
        {"name": "starts_with_cap", "confidence": 1.0},
        {"name": "ends_with_colon", "confidence": 1.0},
        {"name": "contains_year", "confidence": 75},
        {"name": "ends_with_year", "confidence": 0.9},
        {"name": "starts_with_single_letter", "confidence": 1.0},
    ]
    for label in expected_labels:
        assert label in labels

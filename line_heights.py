"""
This module provides functions for analyzing the line heights of content
represented by ContentDefinitions. It includes functionality to extract line heights,
group them into clusters based on similarity.
"""

from typing import List, Tuple, TypeAlias, TypeVar

import numpy as np
from kimiworker import ContentDefinition
from numpy.typing import NDArray


def get_line_heights(content: list[ContentDefinition]) -> list[float]:
    """
    Extracts the line height values from a list of content definitions.

    Args:
        - content (list[dict]): A list of ContentDefinitions.

    Returns:
        - list[float]: A list of line height values as floats.

    """
    return [
        float(content.get("attributes", {}).get("lineHeight", 0))
        for content in content
        if isinstance(content.get("attributes", {}).get("lineHeight"), (float, int))
    ]


TNum = TypeVar("TNum", int, float)


def group_line_heights(line_heights):
    """
    Group line heights into clusters based on a dynamic threshold that considers both
    the median absolute deviation (MAD) and a small percentage of the overall range.

    Args:
        line_heights (List[float]): A list of line heights.

    Returns:
        List[np.ndarray]: A list of numpy arrays, each containing grouped line heights.
    """
    if not line_heights:
        return []

    # Convert the list of line heights to a numpy array and sort it
    line_heights_sorted = np.sort(np.array(line_heights))

    # Calculate the median of the sorted line heights
    median = np.median(line_heights_sorted)

    # Calculate the median absolute deviation relative to the median
    mad = np.median(np.abs(line_heights_sorted - median))

    data_range = line_heights_sorted.max() - line_heights_sorted.min()
    dynamic_threshold = 0.25 * data_range

    # Define a threshold as the larger of the MAD or dynamic threshold
    threshold = max(mad, dynamic_threshold)

    # Calculate the differences between consecutive sorted heights
    differences = np.diff(line_heights_sorted)

    # Find indices where the difference exceeds the threshold
    split_indices = np.where(differences > threshold)[0] + 1

    # Split the sorted heights into groups
    return np.split(line_heights_sorted, split_indices)


LineHeightData: TypeAlias = Tuple[
    int,  # amount_of_groups
    NDArray[np.float64],  # most_common_heights
    NDArray[np.float64],  # largest_heights
    NDArray[np.float64],  # least_common_heights
]


def analyse_line_heights(content: List[ContentDefinition]) -> LineHeightData:
    """
    Analyzes line heights distribution in document content.

    Takes a list of content items and analyzes their line heights to identify groups
    of similar heights. Finds the most common, largest, and least common height groups
    used across the document.

    Args:
        content: List of content items containing line height information

    Returns:
        LineHeightData tuple containing:
        - Number of distinct line height groups found
        - Array of heights in most frequently occurring group
        - Array of heights in group with largest average height
        - Array of heights in least frequently occurring group
    """
    line_heights = get_line_heights(content)
    if not line_heights:
        return (0, np.array([]), np.array([]), np.array([]))

    grouped_heights = group_line_heights(line_heights)
    if not grouped_heights or len(grouped_heights) == 0:
        return (0, np.array([]), np.array([]), np.array([]))

    counts = np.array([len(group) for group in grouped_heights])
    averages = np.array([np.mean(group) for group in grouped_heights])

    most_common_idx = np.argmax(counts)
    largest_idx = np.argmax(averages)
    least_common_idx = np.argmin(counts)

    return (
        len(grouped_heights),
        np.array(grouped_heights[most_common_idx]),
        np.array(grouped_heights[largest_idx]),
        np.array(grouped_heights[least_common_idx]),
    )


def find_line_height_group(combined_stats, target_line_height):
    """
    Finds the group average of the group to which a specified line height belongs.

    Args:
        - combined_stats (list of tuples): Output from analyse_line_heights, containing groups, their averages,
          and counts. target_line_height (float): The line height for which to find the group average.

    Returns:
        - float: The average line height of the group containing the target line height, or None if not found.
    """
    for group, avg, _ in combined_stats:
        if target_line_height in group:
            return avg
    return None

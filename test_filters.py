import pytest
from filters import get_highest_positioned_content
from kimiworker import ContentDefinition


def test_get_highest_positioned_content_with_empty_list():
    """Test get_highest_positioned_content with an empty list."""
    empty_list: list[ContentDefinition] = []

    with pytest.raises(AssertionError):
        get_highest_positioned_content(empty_list)


def test_get_highest_positioned_content_with_single_item():
    """Test get_highest_positioned_content with a single item."""

    single_item: ContentDefinition = {
        "classification": "text",
        "confidence": 0.9,
        "bbox": {"top": 0.1, "left": 0.1, "right": 0.2, "bottom": 0.2},
        "attributes": {"font_size": 12, "color": "black"},
        "labels": [{"name": "paragraph", "confidence": 0.8}],
        "children": None,
    }
    single_item_list = [single_item]

    result = get_highest_positioned_content(single_item_list)

    assert result == single_item


def test_get_highest_positioned_content_with_multiple_items():
    """Test get_highest_positioned_content with multiple items."""

    content_list: list[ContentDefinition] = [
        {
            "classification": "text",
            "confidence": 0.9,
            "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
            "attributes": {"font_size": 12, "color": "black"},
            "labels": [{"name": "paragraph", "confidence": 0.8}],
            "children": None,
        },
        {
            "classification": "text",
            "confidence": 0.95,
            "bbox": {"top": 0.2, "left": 0.3, "right": 0.4, "bottom": 0.4},
            "attributes": {"font_size": 16, "color": "purple"},
            "labels": None,
            "children": None,
        },
        {
            "classification": "text",
            "confidence": 0.85,
            "bbox": {"top": 0.1, "left": 0.2, "right": 0.3, "bottom": 0.3},
            "attributes": {"font_size": 16, "color": "blue"},
            "labels": [{"name": "heading", "confidence": 0.9}],
            "children": None,
        },
    ]

    result = get_highest_positioned_content(content_list)

    assert (
        result == content_list[2]
    ), "Expected the content with the minimum top position"

import sys
import os
from typing import List
from cv2.typing import MatLike
import numpy as np
from numpy.typing import NDArray
from shapely import STRtree
from kimiworker import (
    JobHandler,
    WorkerJob,
    PublishMethod,
    KimiLogger,
    Worker,
    ContentDefinition,
    ContentLabel,
    BoundingBox,
    ContentAttribute,
    RemoteFileStorageInterface,
)
from bbox import bounding_box_to_polygon, get_intersection_ratio
from colors import (
    detect_background_color,
    get_top_colors,
    is_black_strict,
    rgb_to_hex,
    uniform_quantize_image,
)
from filters import get_highest_positioned_content
from grid import pairwise_grid_iterator, points_to_grid_coordinates_by_row
from image import (
    get_image_dimensions,
    process_image,
)
from line_height_labels import get_line_height_labels
from line_heights import analyse_line_heights, LineHeightData

from lines import (
    get_intersections,
    group_intersections,
)
from neighbor_labels import get_neighbor_labels
from page_boundary_labels import get_page_boundary_labels
from text_labels import get_text_labels
from utils import (
    calculate_mean_confidence,
    extract_best_attribute_value,
    filter_content_result,
    get_content_definition_float_attribute,
)

numberOfConcurrentJobs = int(os.getenv("CONCURRENT_JOBS", "1"))
COLOR_TOLERANCE = 10


def get_content_result(image, interpreted_content, grouped_intersections):
    """
    Generates content regions from image by analyzing grid coordinates and OCR text overlaps.

    Takes image grid intersections and OCR text data to create content regions. Extends regions
    horizontally when text overlaps between adjacent cells. Labels regions based on text properties,
    colors, and layout characteristics.

    Args:
        image: Input image to analyze
        interpreted_content: List of OCR text regions with positions and attributes
        grouped_intersections: Grid intersection points that define region boundaries

    Returns:
        List of content region definitions with bounding boxes, labels and attributes
    """
    image_width, image_height = get_image_dimensions(image)
    quantized_image = uniform_quantize_image(image)
    bg_color = detect_background_color(quantized_image)

    grid_coordinates = points_to_grid_coordinates_by_row(grouped_intersections)
    text_polygons = [
        bounding_box_to_polygon(c.get("bbox")) for c in interpreted_content
    ]
    text_boxes_tree = STRtree(text_polygons)

    content_result: List[ContentDefinition] = []
    prev_overlapping: set[int] = set()

    for i, left, top, right, bottom, _, _ in pairwise_grid_iterator(grid_coordinates):
        bbox: BoundingBox = {
            "top": float(top / image_height),
            "left": float(left / image_width),
            "right": float(right / image_width),
            "bottom": float(bottom / image_height),
        }

        box_polygon = bounding_box_to_polygon(bbox)
        overlapping_indices: NDArray[np.int64] = text_boxes_tree.query(box_polygon)
        overlapping: set[int] = set(
            i
            for i in overlapping_indices
            if get_intersection_ratio(interpreted_content[i].get("bbox"), bbox) > 0.3
        )

        if not overlapping:
            continue

        if content_result and (overlapping & prev_overlapping):
            # Expand both horizontally and vertically
            content_result[-1]["bbox"]["right"] = max(
                content_result[-1]["bbox"]["right"], bbox["right"]
            )
            content_result[-1]["bbox"]["bottom"] = max(
                content_result[-1]["bbox"]["bottom"], bbox["bottom"]
            )
        else:
            box_image = quantized_image[top:bottom, left:right]
            labels, attributes = generate_labels(
                overlapping,
                interpreted_content,
                bbox,
                box_image,
                text_boxes_tree,
                bg_color,
                analyse_line_heights(interpreted_content),
            )

            content_result.append(
                {
                    "classification": "application/x-nldoc.region",
                    "bbox": bbox,
                    "confidence": 100,
                    "attributes": attributes,
                    "labels": labels,
                    "children": [],
                }
            )

        prev_overlapping = overlapping

    return content_result


def generate_labels(
    overlapping_indices: set[int],
    interpreted_content: list[ContentDefinition],
    bbox: BoundingBox,
    box_image: MatLike,
    text_boxes_tree: STRtree,
    bg_color: np.ndarray,
    line_height_data: LineHeightData,
) -> tuple[list[ContentLabel], dict[str, ContentAttribute]]:
    """
    Generates labels and attributes for a content region based on its characteristics.

    Analyzes text content, colors, line heights, and positioning to generate descriptive labels
    and metadata attributes. Checks for background colors, text styles, boundary positions etc.

    Args:
        overlapping_indices: Indices of OCR content overlapping this region
        interpreted_content: Full list of OCR content regions
        bbox: Bounding box of the region
        box_image: Image data for the region
        text_boxes_tree: Spatial index of text boxes
        bg_color: Background color of the document
        line_height_data: Analyzed line height statistics

    Returns:
        Tuple of (labels, attributes) describing the region's characteristics

    Raises:
        ValueError: If no overlapping content is found
    """

    if not overlapping_indices or len(overlapping_indices) == 0:
        raise ValueError("No overlapping content found")

    if len(overlapping_indices) > len(interpreted_content):
        raise ValueError("Overlapping indices exceed content length")

    overlapping_content = [interpreted_content[i] for i in overlapping_indices]
    if not overlapping_content:
        raise ValueError("No content found for overlapping indices")

    labels: list[ContentLabel] = []
    attributes: dict[str, ContentAttribute] = {}

    overlapping_content = [interpreted_content[i] for i in overlapping_indices]
    highest_content = get_highest_positioned_content(overlapping_content)

    labels.append({"name": "text", "confidence": highest_content.get("confidence")})

    text = highest_content.get("attributes", {}).get("text", "")
    line_height = get_content_definition_float_attribute(highest_content, "lineHeight")

    row_colors, _ = get_top_colors(box_image)
    if len(row_colors) > 1:
        # Calculate absolute difference between background colors
        color_diff = np.abs(row_colors[0] - bg_color).max()

        if color_diff > COLOR_TOLERANCE:
            labels.append({"name": "has_background_color", "confidence": 0.8})
            attributes["backgroundColor"] = rgb_to_hex(row_colors[0])

        if np.array_equal(row_colors[0], bg_color) and not is_black_strict(
            row_colors[1]
        ):
            labels.append({"name": "text_color_not_black", "confidence": 1.0})
            attributes["textColor"] = rgb_to_hex(row_colors[1])

    amount_of_groups, most_common_heights, largest_heights, least_common_heights = (
        line_height_data
    )

    labels.extend(
        [
            *get_text_labels(text),
            *get_neighbor_labels(text_boxes_tree, bbox),
            *get_line_height_labels(
                line_height,
                amount_of_groups,
                most_common_heights,
                largest_heights,
                least_common_heights,
            ),
            *get_page_boundary_labels(bounding_box_to_polygon(bbox)),
        ]
    )

    return labels, attributes


def get_job_handler() -> JobHandler:
    """Get the job handler"""

    def job_handler(
        log: KimiLogger,
        job: WorkerJob,
        job_id: str,
        local_file_path: str,
        remote_file_storage: RemoteFileStorageInterface,
        publish: PublishMethod,
    ):
        """Handle getting page regions from page image"""

        # Get the list of interpreted content values, if any
        best_value = extract_best_attribute_value(job, "interpretedContent")
        if not best_value:
            log.error("No best value found.")
            publish("contentResult", [], success=False, confidence=0)
            return

        interpreted_content: list[ContentDefinition] = best_value.get(
            "contentResult", []
        )

        try:
            image, filtered_lines = process_image(local_file_path)
        except ValueError as e:
            log.error(f"Image processing failed: {e}")
            publish("contentResult", [], success=False, confidence=0)
            return

        image_width, image_height = get_image_dimensions(image)

        # Get intersections of lines
        intersections = get_intersections(
            interpreted_content, filtered_lines, image_width, image_height
        )

        # Group intersection points to remove noise
        grouped_intersections = group_intersections(intersections)

        # Content result
        content_result = get_content_result(
            image,
            interpreted_content,
            grouped_intersections,
        )

        # Filter items without labels
        content_result = filter_content_result(content_result)

        # Calculate the mean confidence
        mean_confidence = calculate_mean_confidence(content_result)

        log.info("Job done")

        publish(
            "contentResult",
            content_result,
            success=True,
            confidence=mean_confidence,
        )

    return job_handler


if __name__ == "__main__":
    try:  # pragma: no cover
        worker = Worker(get_job_handler(), "worker-page-regions-image-processing", True)
        worker.start(numberOfConcurrentJobs)

    except KeyboardInterrupt:  # pragma: no cover
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

"""
Module for working with content definitions.

This module provides utility functions for working with content definitions,
which represent elements or objects within a document or page. It includes
a function to retrieve the content that is positioned highest on the page.
"""

from kimiworker import ContentDefinition


def get_highest_positioned_content(
    content: list[ContentDefinition],
) -> ContentDefinition:
    """
    Get the content that is positioned highest on the page.

    Args:
        content (list[ContentDefinition]): A list of content definitions.

    Returns:
        ContentDefinition: The content that is positioned highest on the page.
    """
    # Ensure that the list of content is not empty
    assert len(content) > 0, "Content list must not be empty"

    # Find the content with the minimum top position
    highest_positioned_content = min(content, key=lambda x: x.get("bbox").get("top"))

    return highest_positioned_content

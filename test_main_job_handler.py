import pytest
import numpy as np
from unittest.mock import patch, Mock
from kimiworker import WorkerJob

from main import get_job_handler


@pytest.mark.asyncio
async def test_job_handler():
    mock_log = Mock()
    mock_job: WorkerJob = {
        "recordId": "str",
        "bucketName": "str",
        "filename": "str",
        "attributes": {
            "interpretedContent": {
                "name": "interpretedContent",
                "values": [{"contentResult": []}],
            }
        },
    }
    mock_publish = Mock()
    mock_storage = Mock()

    with patch("main.process_image") as mock_process, patch(
        "main.get_intersections"
    ) as mock_intersections, patch("main.group_intersections") as mock_group, patch(
        "main.get_content_result"
    ) as mock_content, patch(
        "main.filter_content_result"
    ) as mock_filter, patch(
        "main.calculate_mean_confidence"
    ) as mock_confidence:

        mock_process.return_value = (np.zeros((100, 100, 3)), [])
        mock_intersections.return_value = []
        mock_group.return_value = []
        mock_content.return_value = []
        mock_filter.return_value = []
        mock_confidence.return_value = 0.9

        handler = get_job_handler()
        handler(mock_log, mock_job, "test-id", "test.jpg", mock_storage, mock_publish)

        mock_publish.assert_called_once()


def test_job_handler_no_content():
    mock_log = Mock()
    mock_job: WorkerJob = {"attributes": []}
    mock_publish = Mock()

    handler = get_job_handler()
    handler(mock_log, mock_job, "test-id", "test.jpg", Mock(), mock_publish)

    mock_publish.assert_called_with("contentResult", [], success=False, confidence=0)


def test_job_handler_image_processing_error():
    mock_log = Mock()
    mock_job: WorkerJob = {
        "attributes": [{"name": "interpretedContent", "value": {"contentResult": []}}]
    }
    mock_publish = Mock()

    with patch("main.process_image", side_effect=ValueError("Test error")):
        handler = get_job_handler()
        handler(mock_log, mock_job, "test-id", "test.jpg", Mock(), mock_publish)

        mock_publish.assert_called_with(
            "contentResult", [], success=False, confidence=0
        )

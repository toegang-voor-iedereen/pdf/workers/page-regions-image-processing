from validations import (
    contains_month,
    contains_year,
    ends_with_colon,
    ends_with_period,
    ends_with_semicolon,
    ends_with_year,
    has_list_item_structure,
    in_range,
    is_lowercase,
    is_numeric,
    is_single_word,
    is_uppercase,
    starts_numeric,
    starts_with_capital,
    starts_with_roman_numeral,
    starts_with_single_letter,
)


def test_in_range():
    """
    Validates that the in_range function accurately determines whether a number is within a specified inclusive range.
    """
    assert in_range(5, 10) is True, "5 should be considered within the range up to 10."
    assert (
        in_range(10, 10) is True
    ), "10, as the boundary value, should be considered within range."
    assert (
        in_range(-1, 10) is False
    ), "-1 should be considered outside the range up to 10."
    assert (
        in_range(11, 10) is False
    ), "11 should be considered outside the range up to 10."


def test_is_numeric():
    """
    Ensures the is_numeric function correctly identifies numeric values, irrespective of their type (int or str).
    """
    assert is_numeric(123) is True, "Integer values are numeric."
    assert (
        is_numeric("123") is True
    ), "String representations of integer numbers are considered numeric."
    assert (
        is_numeric("3.14") is True
    ), "String representations of float numbers are considered numeric."
    assert (
        is_numeric("NaN") is False
    ), "'NaN' string is not considered as numeric despite being a valid float."
    assert is_numeric("abc") is False, "Alphabetic strings are not numeric."


def test_starts_numeric():
    """
    Tests the starts_numeric function to check if it accurately identifies strings starting with a numeric character.
    """
    assert (
        starts_numeric("4th of July") is True
    ), "String '4th of July' starts with a numeric character."
    assert (
        starts_numeric("999 is a big number") is True
    ), "String '999 is a big number' starts with a numeric character."
    assert (
        starts_numeric("twenty20") is False
    ), "String 'twenty20' does not start with a numeric character."
    assert (
        starts_numeric("1969 was a great year") is True
    ), "String '1969 was a great year' starts with a numeric character."


def test_starts_with_capital():
    """
    Verifies if starts_with_capital function correctly identifies strings that start with a capital letter.
    """
    assert (
        starts_with_capital("Capital") is True
    ), "String starting with a capital letter."
    assert (
        starts_with_capital(" Space before") is True
    ), "Leading spaces before a capital letter should not fail."
    assert (
        starts_with_capital("not Capital") is False
    ), "String does not start with a capital letter."
    assert (
        starts_with_capital("123 numbers") is False
    ), "String starts with numbers, not a capital letter."


# Test for is_single_word function
def test_is_single_word():
    """
    Confirms that is_single_word function accurately identifies single words, considering spaces and underscores.
    """
    assert is_single_word("Word") is True, "A single word without any spaces."
    assert (
        is_single_word("two words") is False
    ), "Multiple words separated by spaces should not be considered single."
    assert (
        is_single_word("single_word") is True
    ), "Underscores do not invalidate a single word."


def test_is_uppercase():
    """
    Tests the is_uppercase function to ensure it identifies strings that are entirely uppercase correctly.
    """
    assert is_uppercase("UPPERCASE") is True, "String entirely in uppercase."
    assert (
        is_uppercase("MixedCase") is False
    ), "String with mixed case is not entirely uppercase."
    assert is_uppercase("lowercase") is False, "String in lowercase is not uppercase."
    assert is_uppercase("8") is False, "String that is fully numeric is not uppercase."
    assert (
        is_uppercase("8 FOO") is True
    ), "String that starts numeric and rest is fully uppercase."
    assert is_uppercase(8) is False, "A numeric input is not uppercase."


def test_is_lowercase():
    """
    Ensures the is_lowercase function accurately identifies strings that are entirely lowercase.
    """
    assert is_lowercase("lowercase") is True, "String entirely in lowercase."
    assert (
        is_lowercase("MixedCase") is False
    ), "String with mixed case is not entirely lowercase."
    assert is_lowercase("UPPERCASE") is False, "String in uppercase is not lowercase."
    assert is_lowercase("8") is False, "String that is fully numeric is not lowercase."
    assert (
        is_lowercase("8 foo") is True
    ), "String that starts numeric and rest is fully lowercase."
    assert is_lowercase(8) is False, "A numeric input is not lowercase."


def test_ends_with_colon():
    """
    Verifies that ends_with_colon function correctly identifies strings that end with a colon.
    """
    assert (
        ends_with_colon("This ends with a colon:") is True
    ), "String ends with a colon."
    assert (
        ends_with_colon("This does not") is False
    ), "String without a colon at the end."
    assert (
        ends_with_colon("Almost there;") is False
    ), "Semicolon is not the same as a colon."


def test_ends_with_semicolon():
    """
    Tests the ends_with_semicolon function to check for strings ending specifically with a semicolon.
    """
    assert (
        ends_with_semicolon("Statement ends here;") is True
    ), "String ends with a semicolon."
    assert (
        ends_with_semicolon("Incomplete statement:") is False
    ), "Colon is not the same as a semicolon."
    assert (
        ends_with_semicolon("No punctuation") is False
    ), "String without ending punctuation."


def test_ends_with_period():
    """
    Confirms that ends_with_period function accurately identifies strings that conclude with a period.
    """
    assert (
        ends_with_period("This sentence ends with a period.") is True
    ), "String ends with a period."
    assert (
        ends_with_period("This does not end with a period") is False
    ), "String without a period at the end."
    assert (
        ends_with_period("Question mark?") is False
    ), "Other punctuation marks are not considered periods."


def test_has_list_item_structure():
    """
    Verifies if has_list_item_structure function correctly identifies strings formatted as list items.
    """
    assert has_list_item_structure("a. Foo") is True, "Alphanumeric list"
    assert has_list_item_structure("15. Item") is True, "Numerical list item."
    assert has_list_item_structure("-Foo") is True, "Bulleted list item without space."
    assert has_list_item_structure("- Bullet point") is True, "Bulleted list item."
    assert has_list_item_structure("* Starred item") is True, "Starred list item."
    assert (
        has_list_item_structure("Just text") is False
    ), "Regular text is not a list item."


def test_contains_year():
    """
    Tests the contains_year function to ensure it identifies strings that contain a year within them.
    """
    assert (
        contains_year("In 1999, something happened.") is True
    ), "String contains a valid year."
    assert (
        contains_year("The year 20202 is not valid.") is False
    ), "String contains an invalid year format."
    assert contains_year("Year 1984") is True, "String with a year at the beginning."


def test_ends_with_year():
    """
    Confirms that ends_with_year function accurately identifies strings that end with a valid year, possibly followed by punctuation.
    """
    assert (
        ends_with_year("Event happened in 1999.") is True
    ), "String ends with a year and period."
    assert ends_with_year("It was back in 1984") is True, "String ends with a year."
    assert (
        ends_with_year("This is not a year 100000") is False
    ), "String with an invalid year format."


def test_contains_month():
    """
    Ensures the contains_month function correctly identifies strings that contain month names.
    """
    assert (
        contains_month("It was a cold January.") is True
    ), "String contains a month name."
    assert (
        contains_month("April showers bring May flowers.") is True
    ), "String contains multiple month names."
    assert (
        contains_month("This does not have a month") is False
    ), "String without a month name."


def test_starts_with_single_letter():
    """
    Ensures the starts_with_single_letter function correctly identifies strings that start with a single letter
    """

    assert (
        starts_with_single_letter("A. Cool thing") is True
    ), "String starts with a capital single letter followed by a dot."

    assert (
        starts_with_single_letter("a. Cool thing") is True
    ), "String starts with a lower case single letter followed by a dot."

    assert (
        starts_with_single_letter("A Cool thing") is True
    ), "String starts with a capital single letter."

    assert (
        starts_with_single_letter("a Cool thing") is True
    ), "String starts with a lower case single letter."

    assert (
        starts_with_single_letter("ai Cool thing") is False
    ), "String does not start with a single letter."

    assert (
        starts_with_single_letter("AI Cool thing") is False
    ), "String does not start with a single letter."


def test_starts_with_roman_numeral():
    """
    Ensures the starts_with_roman_numeral function correctly identifies strings that start with a roman numeral
    """

    assert (
        starts_with_roman_numeral("I. Cool!") is True
    ), "String starts with roman numeral that ends with a dot"

    assert (
        starts_with_roman_numeral("IV. Cool!") is True
    ), "String starts with roman numeral that ends with a dot"

    assert (
        starts_with_roman_numeral("IV Cool!") is False
    ), "String starts with roman numeral, but has no dot"

    assert (
        starts_with_roman_numeral("MCCCXLIV. Cool!") is True
    ), "String starts with roman numeral that ends with a dot"

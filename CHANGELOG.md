# Changelog

## [1.14.5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.14.4...1.14.5) (2025-03-06)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing docker tag to v1.14.4 ([61ad70f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/61ad70f4eaa932db89aaa280730c5263e15d86cc))

## [1.14.4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.14.3...1.14.4) (2025-03-06)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing docker tag to v1.14.3 ([f0db72a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/f0db72af7716d2509c3f6fef3683d21e8a0ed678))

## [1.14.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.14.2...1.14.3) (2025-03-05)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing docker tag to v1.14.2 ([dfddcd2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/dfddcd2a3d59a6b4f8024a6a591ee423d26d5ec3))

## [1.14.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.14.1...1.14.2) (2025-03-05)


### Bug Fixes

* **deps:** update dependency numpy to v2.2.3 ([82bee75](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/82bee75e3dc4f8aa632b764dd99c408de75a4a8c))

## [1.14.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.14.0...1.14.1) (2025-01-20)


### Bug Fixes

* update base worker ([5f14c5a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/5f14c5af6e96fd58bc92444b30135bfd8fcd51d0))

# [1.14.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.13.2...1.14.0) (2024-12-16)


### Features

* **deps:** kimiworker 4.4.0, removed minio dependency ([e0578b9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/e0578b9ff4d0c1dbfb6a6b77649268064cbda89d))

## [1.13.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.13.1...1.13.2) (2024-12-03)


### Bug Fixes

* default to 0 mean confidence when there are no items ([105c0ac](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/105c0ac604820275963cf53a52098d39355a9e82))

## [1.13.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.13.0...1.13.1) (2024-12-02)


### Bug Fixes

* correctly formatted fallback lines ([01a3970](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/01a397085f45ceb0aeff2981b80d56ea27fe7ba2))

# [1.13.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.12.1...1.13.0) (2024-12-02)


### Bug Fixes

* expand significantly overlapping boxes horizontally and vertically ([fff52c9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/fff52c94c4951d0951c1839881beed8f98485f74))


### Features

* add function to calculate intersection ratio ([68df8ff](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/68df8ff4b0e22f871104b43ad5c8df1a5a62b15b))
* add function to detect if a color is strictly black ([94f399a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/94f399a22009add8db7c855426c1c7f82a0f94be))
* label confidence between 0 and 1 ([0bd6c34](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/0bd6c345a081b637517c6e9ef21b5bf35c13814d))

## [1.12.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.12.0...1.12.1) (2024-11-28)


### Bug Fixes

* improve performance ([3fa7ef3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/3fa7ef3af2c63ed6d42bc02edfb42741ff32b8a0))

# [1.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.11.0...1.12.0) (2024-11-19)


### Features

* force release with kimiworker v3.6.0 ([a1fcaca](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/a1fcaca173a49216ba73cfcc178874cde9b76b4b))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.10.0...1.11.0) (2024-11-14)


### Bug Fixes

* removed nldocspec references in notebook ([e706a85](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/e706a85b23dd7f66519a5ba8a016adbfcbc97566))
* removed nldocspec references in notebook ([b1adbbd](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/b1adbbdd2434231e7aa73d99091bb94d5b3615fc))
* revert to ContentDefinition ([125364d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/125364d9e37054b02a5b9b89fddd24d9f6b79ed4))
* revert to ContentDefinition ([eec0ed4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/eec0ed4313555de90942de71c7dd39796d3fcedc))


### Features

* handle empty arrays and defaults ([fc1e4fb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/fc1e4fb43dd5579aa0df0c42586f5302d91710cf))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.9.2...1.10.0) (2024-11-06)


### Features

* kimiworker 3.3.1 ([46df950](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/46df950282c5724eb08f7c8ec174d84e670ff8fe))
* kimiworker 3.4.0-alpha.2 to use ssl for MinIO without using nldocspec ([0d3a05f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/0d3a05f1bba0ed6a6fc9bf2f39d1eca647f70e11))
* kimiworker 3.5.0 ([92dd591](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/92dd591da65e05d971ae496172ab0bf19f8d1522))

## [1.9.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.9.1...1.9.2) (2024-10-22)


### Bug Fixes

* corrected all attribute and label uris ([510ae2a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/510ae2a04dc935bcccc245324f3055a77bd98854))

## [1.9.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.9.0...1.9.1) (2024-10-16)


### Bug Fixes

* read bounding box from object descriptor and convert to dict ([d59fddb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/d59fddb575ca6bcb8dd2e446a6d876c584712791))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.8.0...1.9.0) (2024-09-11)


### Features

* added volume mount for /tmp ([1ad153f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/1ad153f7db73d624df3b803a19aaed114932d753))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.7.0...1.8.0) (2024-09-04)


### Features

* added securityContext to container and initContainers ([2c71b1a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/2c71b1a904952ed4a3609cbdeb1ee1b31db8d518))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.6.1...1.7.0) (2024-08-28)


### Features

* use new kimiworker to connect to station 4.0.0 and up ([2701bd3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/2701bd3ced179e587c0d1263be971334ddf7f30f))

## [1.6.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.6.0...1.6.1) (2024-08-27)


### Bug Fixes

* add example ([e5d9f43](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/e5d9f43790e78946be87ee9fc2266726cb6acea6))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.5.0...1.6.0) (2024-06-27)


### Bug Fixes

* disable bold detection for now ([de63df6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/de63df6eb4df2c47c5c47eb763f0d26b315fba97))


### Features

* add single letter and roman numeral validations ([09997eb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/09997ebd7487a984856195e746c5db8fede318a6))
* support initContainers in helm values ([1d690b2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/1d690b2f4c17f8561fc9419a9ec3aef1db8c3cda))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.4.0...1.5.0) (2024-06-05)


### Features

* support initContainers in helm values ([08f3cca](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/08f3cca67faa418202674bcff542aba407ec574a))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.3.0...1.4.0) (2024-06-04)


### Bug Fixes

* check list item length ([40bd042](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/40bd04299cbf251352cc8e9a88708db6ba088b75))


### Features

* correct worker name in chart ([08e9b42](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/08e9b4237ffaec24d9ad7261272a53359d6bf9ce))

# [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.2.1...1.3.0) (2024-05-13)


### Features

* use content objects from kimiworker ([388b3d0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/388b3d0fe2583b824dec84484c1108431acf6a87))

## [1.2.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.2.0...1.2.1) (2024-05-06)


### Bug Fixes

* import kimilogger ([14c8cd9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/14c8cd977eaeb5e6f0f809cc72835f022c87167c))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/1.1.1...1.2.0) (2024-05-06)


### Features

* allow float as content attribute ([3dfd71f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/3dfd71fbd3b217240e16fcb5fc624e6e16268ff0))
* border polygons utility ([678a78a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/678a78ad3ac27dceb55d58151a0a66b2d79bcde4))
* cache bounding box conversion ([2a35937](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/2a35937fc0088dd535e2753169c77c5eb43440be))
* **deps:** update dependency kimiworker to v2 ([6bd4664](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/6bd4664d410d5b5babc95259106c8a66de707f2f))
* detect outliers in a list ([9c847e4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/9c847e458819612d813c926a41af2721144d45c1))
* find line height group ([c6caac2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/c6caac29c612c9d1cfd12f9a0a5fc752a7a6e003))
* generator functions for getting color coverage ([158e0f8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/158e0f84a226c7daf7671420a1b7d6fb4f97ad21))
* get neighbor labels ([1f30a42](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/1f30a42df8720f64dd3e5143db07579f59db3970))
* getting line height labels ([c4fbd71](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/c4fbd7161f8043fd2cbbc3370524dff25b58a0d7))
* grid coordinates iterator ([aafd7f2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/aafd7f2bbd0613fac47e2ea149e37f28bfaf0b62))
* highlight bold regions ([f58ec66](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/f58ec66e1e8c546d97a276a8eaa9afe4f3c4e787))
* ignore some Flake8 rules ([b9a31ab](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/b9a31ab7dd5ef093357b0601baba7e2eb2f8e661))
* merge branch 'renovate/kimiworker-2.x' into 'develop' ([5e963f3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/5e963f330df3e5a2d7c830539c49489ce6eed246))
* page boundary labels ([4dfab4b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/4dfab4b1649e38bae05b3ffdc3ff520f1a5f1448))
* remove bold detection based on color coverage ([19e9534](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/19e9534faaa2aa844648f65e3d11e5c7d89131f7))
* restructure main.py ([df4f0d8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/df4f0d889810f2de35ccfdad79c7be199de8cf04))
* utility for calculating mean confidence ([58a8b2e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/58a8b2ebfbbcc33a023cc1c021c9fcb3bc3d652b))
* utility for filtering the content result ([7c615a3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/7c615a3d6d9bac9af8efe3a46140eb71a109e5d8))
* utility functions for extracting color information from images ([ba4a43e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/ba4a43eb261ca19bf133c4dd6511970dafb5e652))

## [1.1.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/v1.1.0...1.1.1) (2024-04-24)


### Bug Fixes

* force a release with the new pipeline ([e5d43b6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/e5d43b662dcdf7f88b5ea54b57a77fe9a1ff5603))

# [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/compare/v1.0.0...v1.1.0) (2024-04-09)


### Bug Fixes

* floor cluster centroid coordinate before casting to int ([e0e0d29](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/e0e0d29046877e7289826d2bd11e7fc8ad4fc7ee))
* handle cases where lines are parallel ([37c75b4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/37c75b4c9e606ddeb82abb70198cf6e61697d785))
* intersection could be None for perpendicular lines ([756ab33](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/756ab33b2ee2c8695a2f05ff31541cfb9a00174c))


### Features

* add description to filters ([a82d6e6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/a82d6e6e88d98a25520119e9613a32f9062fc2e7))
* add descriptions to bbox functions ([a83b704](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/a83b704b2eb4a95f23c02c9980c445787993af2c))
* add descriptions to image functions ([4699bbb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/4699bbb4b1557ab3bc3f8690e0419901b066e48d))
* add descriptions to line functions ([e8f5913](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/e8f591313f618df11f23df7419cde9136726892c))
* add descriptions to text validations ([46855f2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/46855f2e9c1f6e195109e6198dbf7d191127b155))
* add in_range validation ([2cef81a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/2cef81aad4ee5d314e6cf7347b613b2b1673a7e2))
* add tests for filters ([cb5928f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/cb5928f62ebb6f0b112f3307c439c44b93a05610))
* add tests for line operations ([4738748](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/4738748f35baec38579675677522a8b60e38daea))
* add validations ([0b38556](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/0b3855644b8a8580c0a92a04c61e96e700e85d9e))
* check isnan ([d61f056](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/d61f056f82d482a9927a44f060d6a51952a668b4))
* detect image labels ([3ef0f68](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/3ef0f683d2ad4c48b1f782637332006e34e90e97))
* ensure grid coordinates are unique ([d114e75](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/d114e7564e1b7d739339a9c3e674784b5c00ad92))
* improve date regex ([82c6436](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/82c6436e2a87907ed7030ead1ae8ade038bf91ea))
* move sidelines ([a404fce](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/a404fce52f6da6612c523803ebad593aeacefb21))
* refactor line angles ([949b733](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/949b7335d917afdb90e67ed9f6cc65518e53164a))
* test extract best value ([27454cc](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/27454cc072655db20477828ffd8bc87bcbe8d62d))
* update pipeline ([66a471d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing/commit/66a471dc057a82822f5d8c12e89c4c0aa61038b3))

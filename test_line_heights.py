import numpy as np
from kimiworker import ContentDefinition
from unittest.mock import patch

from line_heights import (
    analyse_line_heights,
    find_line_height_group,
    get_line_heights,
    group_line_heights,
)


def content_definition_with_line_height(line_height: int | str) -> ContentDefinition:
    """
    Generate a content definition with specified line_height
    """
    return {
        "classification": "text",
        "confidence": 0.9,
        "bbox": {"top": 0.3, "left": 0.1, "right": 0.2, "bottom": 0.4},
        "attributes": {"lineHeight": line_height},
        "labels": [{"name": "paragraph", "confidence": 0.8}],
        "children": None,
    }


def test_get_line_heights():
    mock_content = [
        content_definition_with_line_height(1),
        content_definition_with_line_height("1.3"),
        content_definition_with_line_height(2),
        {**content_definition_with_line_height(""), "attributes": {}},
    ]
    assert get_line_heights(mock_content) == [1, 2.0]


def test_empty_group_line_heights():
    assert len(group_line_heights([])) == 0


def test_group_line_heights():
    line_heights = [1, 2, 5, 11, 12]
    grouped = group_line_heights(line_heights)

    assert len(grouped) == 2
    assert all(isinstance(g, np.ndarray) for g in grouped)
    assert np.array_equal(grouped[0], np.array([1, 2, 5]))
    assert np.array_equal(grouped[1], np.array([11, 12]))


def test_analyse_line_heights():
    mock_content = [
        *[content_definition_with_line_height(1)] * 2,
        *[content_definition_with_line_height(5)] * 3,
        content_definition_with_line_height(9),
    ]

    groups, most_common, largest, least_common = analyse_line_heights(mock_content)

    assert groups == 3
    assert np.array_equal(most_common, np.array([5, 5, 5]))
    assert np.array_equal(largest, np.array([9]))
    assert np.array_equal(least_common, np.array([9]))


def test_analyse_line_heights_no_line_heights():
    content = []  # Input can be anything since get_line_heights is mocked

    with patch("line_heights.get_line_heights", return_value=None):
        result = analyse_line_heights(content)

    # Assert that the return matches the expected output when line_heights is None
    assert result[0] == 0
    assert np.array_equal(result[1], np.array([]))
    assert np.array_equal(result[2], np.array([]))
    assert np.array_equal(result[3], np.array([]))


def test_analyse_line_heights_no_group_line_heights():
    content = []  # Input can be anything since get_line_heights is mocked

    with patch(
        "line_heights.get_line_heights",
        return_value=[5, 5, 10, 10, 10, 10, 12, 12, 12, 14, 14, 14, 24],
    ):
        with patch("line_heights.group_line_heights", return_value=None):
            result = analyse_line_heights(content)

    # Assert that the return matches the expected output when line_heights is None
    assert result[0] == 0
    assert np.array_equal(result[1], np.array([]))
    assert np.array_equal(result[2], np.array([]))
    assert np.array_equal(result[3], np.array([]))


def test_find_line_height_group_exact():
    """Test finding exact line height match"""
    stats = [([10.0, 10.5], 10.25, 2)]
    assert find_line_height_group(stats, 10.0) == 10.25


def test_find_line_height_group_multiple():
    """Test with multiple height groups"""
    stats = [([10.0, 10.5], 10.25, 2), ([20.0, 20.5], 20.25, 2)]
    assert find_line_height_group(stats, 20.0) == 20.25


def test_find_line_height_group_not_found():
    """Test when height isn't found in any group"""
    stats = [([10.0, 10.5], 10.25, 2)]
    assert find_line_height_group(stats, 15.0) is None


def test_find_line_height_group_empty():
    """Test with empty stats"""
    assert find_line_height_group([], 10.0) is None

"""
This module provides tools for detecting outliers in a dataset.
"""

from typing import List, TypeVar
import numpy as np


TNum = TypeVar("TNum", int, float)


def detect_outliers(
    items: List[TNum], mode: str = "all", multiplier: float = 4.5
) -> List[TNum]:
    """
    Detects outliers in a list of numerical data based on median and median absolute deviation (MAD).

    Args:
        items (List[TNum]): The list of items (numerical data) to analyze.
        mode (str): The mode of outliers to return. Can be 'all', 'low', or 'high'.
                    Defaults to 'all'.
        multiplier (float): The factor used to multiply the MAD to set the outlier threshold. Defaults to 3.

    Returns:
        List[TNum]: A list of items from the input that are considered outliers according to the specified mode.

    Raises:
        ValueError: If an invalid mode is specified.
    """
    if mode not in ["all", "low", "high"]:
        raise ValueError("Invalid mode specified. Use 'all', 'low', or 'high'.")

    if len(items) < 2:
        return []

    median = np.median(items)
    mad = np.median([abs(x - median) for x in items])

    if mad == 0:
        return []

    # Calculate the threshold using MAD
    threshold = mad * multiplier

    # Calculate deviations based on the median
    deviations = [abs(x - median) for x in items]

    # Identify and return the outliers based on the mode and calculated threshold
    if mode == "all":
        outliers = [item for item, dev in zip(items, deviations) if dev > threshold]
    elif mode == "low":
        outliers = [
            item for item, dev in zip(items, deviations) if (median - item) > threshold
        ]
    elif mode == "high":
        outliers = [
            item for item, dev in zip(items, deviations) if (item - median) > threshold
        ]

    return outliers

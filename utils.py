"""
Utility functions
"""

from typing import Optional
from statistics import mean
from pathdict import PathDict
from kimiworker import WorkerJob, ContentDefinition


def extract_best_attribute_value(job: WorkerJob, attribute: str) -> Optional[dict]:
    """
    Extracts the best value from a WorkerJob attribute based on the best job ID.

    This function attempts to retrieve a specific attribute from a WorkerJob, identifies the best
    job ID for this attribute, and then finds the corresponding value that matches this job ID.

    This functionality should become part of a separate library at some point

    Args:
        job (WorkerJob): A WorkerJob.
        attribute (str): The attribute key whose best value is to be extracted.

    Returns:
        Optional[dict]: The dictionary containing the best attribute value or None if no matching value is found.

    Raises:
        None explicitly, but will return None if the necessary keys are not present or the data is malformed.
    """
    try:
        attributes = PathDict(job.get("attributes", {}))
        best_job_id = attributes[f"{attribute}.bestJobId"]
        values = attributes[f"{attribute}.values"]
        return next((x for x in values or [] if x["jobId"] == best_job_id), None)
    except KeyError:
        return None


def get_content_definition_float_attribute(
    content: ContentDefinition, attribute: str
) -> float:
    """
    Extracts the float value of a specified attribute from a content definition.

    Args:
        content (ContentDefinition): The content definition from which to extract the attribute.
        attribute (str): The name of the attribute to extract.

    Returns:
        float: The float value of the attribute, or 0.0 if the attribute does not exist.
    """
    return float(content.get("attributes", {}).get(attribute, 0))


def get_line_height_group(content: ContentDefinition) -> float:
    """
    Retrieves the 'lineHeightGroup' float attribute from the provided content definition.

    Args:
        content (ContentDefinition): The content definition from which the line height group is extracted.

    Returns:
        float: The value of the line height group, defaulting to 0.0 if not found.
    """
    return get_content_definition_float_attribute(content, "lineHeightGroup")


def get_background_coverage(content: ContentDefinition) -> float:
    """
    Retrieves the 'backgroundCoverage' float attribute from the provided content definition.

    Args:
        content (ContentDefinition): The content definition from which the background coverage is extracted.

    Returns:
        float: The value of the background coverage, defaulting to 0.0 if not found.
    """
    return get_content_definition_float_attribute(content, "backgroundCoverage")


def filter_content_result(content_result: list[ContentDefinition]):
    """
    Filter content result on items that have labels

    Args:
        - content_result (list[ContentDefinition]): List of content definitions

    Returns:
        - list[ContentDefinition]: Filtered list of content definitions (Only content with labels)
    """
    return [
        content for content in content_result if len(content.get("labels") or []) > 0
    ]


def calculate_mean_confidence(content_result: list[ContentDefinition]) -> float:
    """
    Calculate the mean confidence for the content regions found

    Args:
        - content_result (list[ContentDefinition]): List of content definitions

    Returns:
        - float: the mean confidence of all result items
    """
    return (
        mean([content.get("confidence") for content in content_result])
        if content_result
        else 0
    )

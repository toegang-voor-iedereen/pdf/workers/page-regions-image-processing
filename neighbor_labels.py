from typing import cast
from shapely import STRtree, box
from shapely.geometry import Polygon
from kimiworker import BoundingBox, ContentLabel


def get_neighbor_labels(text_boxes_tree: STRtree, bbox: BoundingBox):
    """
    Detects if a text box has neighboring text boxes directly above or below it.

    Uses a small border region (0.005 units) above and below the given bounding box to check
    for adjacent text boxes. Only considers boxes that start within this border region to
    avoid false positives from overlapping boxes.

    Args:
        text_boxes_tree: Spatial index containing text boxes as polygons
        bbox: Bounding box to check for neighbors

    Returns:
        List of labels indicating presence of neighbors (has_neighbor_top, has_neighbor_bottom)
    """
    labels: list[ContentLabel] = []

    top_query = box(bbox["left"], bbox["top"] - 0.005, bbox["right"], bbox["top"])
    bottom_query = box(
        bbox["left"], bbox["bottom"], bbox["right"], bbox["bottom"] + 0.005
    )

    for idx in text_boxes_tree.query(bottom_query):
        other_box = cast(Polygon, text_boxes_tree.geometries[idx])
        _min_x, min_y, _max_x, max_y = other_box.bounds

        if min_y >= bbox["bottom"] and min_y <= bbox["bottom"] + 0.005:
            labels.append({"name": "has_neighbor_bottom", "confidence": 1.0})
            break

    for idx in text_boxes_tree.query(top_query):
        other_box = cast(Polygon, text_boxes_tree.geometries[idx])
        _min_x, min_y, _max_x, max_y = other_box.bounds

        if max_y <= bbox["top"] and max_y >= bbox["top"] - 0.005:
            labels.append({"name": "has_neighbor_top", "confidence": 1.0})
            break

    return labels

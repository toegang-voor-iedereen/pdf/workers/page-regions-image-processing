"""
This module provides utility functions for extracting color information from images. The functions
allow you to identify the most frequent colors in an image, compute the average color based on
frequency, and detect the predominant background color.
"""

from cv2.typing import MatLike
import numpy as np


def get_top_colors(
    image: np.ndarray, amount: int = 10
) -> tuple[np.ndarray, np.ndarray]:
    """
    Extracts the top X most frequent colors from the provided image.

    Args:
        image (np.ndarray): A 3D numpy array representing an RGB image.
        amount (int): The number of top colors to return.

    Returns:
        Tuple[np.ndarray, np.ndarray]: The top colors and their corresponding counts.
    """
    if not (isinstance(image, np.ndarray) and image.ndim == 3 and image.shape[2] == 3):
        raise ValueError("Input must be a 3D numpy array with three channels (RGB).")

    # Flatten the image and count unique colors
    flat_image = image.reshape(-1, 3)
    unique_colors, counts = np.unique(flat_image, axis=0, return_counts=True)

    # Sort the colors by their frequency
    sorted_indices = np.argsort(-counts)
    top_colors = unique_colors[sorted_indices[:amount]].astype(np.int32)
    top_counts = counts[sorted_indices[:amount]].astype(np.int32)

    return top_colors, top_counts


def detect_background_color(image: MatLike) -> np.ndarray:
    """
    Detects the background color of the input image using efficient border sampling.

    Args:
        image (MatLike): The input image to detect the background color from

    Returns:
        np.ndarray: An array representing the background color of the image
    """
    h, w = image.shape[:2]

    # Take border pixels as samples
    border_samples = np.concatenate(
        [
            image[0, :: w // 100],  # top border
            image[-1, :: w // 100],  # bottom border
            image[:: h // 100, 0],  # left border
            image[:: h // 100, -1],  # right border
        ]
    )

    # Get unique colors and their counts
    unique_colors, counts = np.unique(
        border_samples.reshape(-1, 3), axis=0, return_counts=True
    )

    # If dominant color exists (>50% of samples), use it
    if counts[0] / len(border_samples) > 0.5:
        return unique_colors[0]

    # Calculate weighted average manually
    weighted_sum = np.sum(unique_colors * counts[:, np.newaxis], axis=0)
    total_count = np.sum(counts)

    return (weighted_sum / total_count).astype(np.uint8)


def uniform_quantize_image(image: MatLike, colors: int = 64) -> MatLike:
    """
    Apply uniform quantization to an image to reduce its color depth to a specified number of colors.
    This method divides the color space into equal-sized bins, quantizing each RGB channel based on the cube root of
    the specified number of colors, achieving approximately the desired total number of colors.

    Args:
    image (MatLike): An OpenCV image in RGB format (numpy array of type uint8).
    colors (int): Total desired colors in the output image; should be a perfect cube.

    Returns:
    MatLike: The quantized image as a NumPy array in RGB format.
    """
    if colors < 8:
        raise ValueError(
            "Colors must be a value higher than 8 and must be a perfect cube"
        )

    levels = int(np.cbrt(colors))
    if levels**3 != colors:
        raise ValueError("Colors must be a perfect cube")

    if not (isinstance(image, np.ndarray) and image.ndim == 3 and image.shape[2] == 3):
        raise ValueError("Input must be a 3D numpy array with three channels (RGB)")

    bin_size = 255 / (levels - 1)
    return np.array(np.round(image / bin_size) * bin_size, dtype=np.uint8)


def rgb_to_hex(rgb_array: np.ndarray) -> str:
    """
    Convert an RGB array into a hexadecimal color string.

    Args:
        rgb_array (np.ndarray): A NumPy array of three integers, each ranging from 0 to 255,
                                representing the Red, Green, and Blue components of a color.

    Returns:
        str: A string representing the hexadecimal color, formatted as '#RRGGBB'.
    """
    # Ensure the input is a NumPy array with integers (this step is also a basic validation)
    rgb_array = np.array(rgb_array, dtype=int)

    # Check if the array has the correct shape and values
    if rgb_array.shape != (3,):
        raise ValueError("Input array must have exactly three elements.")
    if np.any((rgb_array < 0) | (rgb_array > 255)):
        raise ValueError("Each element in the array must be between 0 and 255.")

    # Convert the RGB components to a hexadecimal string
    return f"#{rgb_array[0]:02x}{rgb_array[1]:02x}{rgb_array[2]:02x}"


def is_black_strict(color: np.ndarray, brightness_threshold=60, max_channel_value=50):
    """
    Determine if a color is "strictly black" based on brightness and channel thresholds.

    Args:
        color (np.ndarray): The color to analyze (BGR).
        brightness_threshold (int): Maximum average brightness for a color to be considered black.
        max_channel_value (int): Maximum value any single channel can have to qualify as black.

    Returns:
        bool: True if the color is considered black, False otherwise.
    """
    # Convert BGR to brightness (average of R, G, B)
    brightness = np.mean(color)

    # Ensure no single channel exceeds max_channel_value
    is_dark = np.all(color <= max_channel_value)

    # Check overall brightness
    return is_dark and brightness <= brightness_threshold

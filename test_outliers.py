import pytest
import numpy as np

from outliers import detect_outliers


def test_empty_list():
    """Testing with an empty list should return an empty list."""
    assert detect_outliers([]) == []


def test_single_value():
    """A single value cannot be an outlier by itself."""
    assert detect_outliers([100]) == []


def test_no_outliers():
    """A list where all items are the same should have no outliers."""
    assert detect_outliers([5, 5, 5, 5, 5]) == []


def test_all_mode():
    """Testing detection in 'all' mode."""
    data = [10, 12, 100, 102, 103, 14, 16]
    outliers = detect_outliers(data, mode="all")
    assert set(outliers) == {103, 100, 102}


def test_low_mode():
    """Testing detection in 'low' mode."""
    data = [10, 12, 14, 16, 100]
    outliers = detect_outliers(data, mode="low")
    assert outliers == []


def test_high_mode():
    """Testing detection in 'high' mode."""
    data = [10, 12, 14, 16, 100]
    outliers = detect_outliers(data, mode="high")
    assert set(outliers) == {100}


def test_invalid_mode():
    """Invalid mode values should raise an exception."""
    with pytest.raises(ValueError):
        detect_outliers([10, 20, 30], mode="medium")


def test_large_dataset():
    """Test function performance with a large dataset."""
    large_data = np.random.normal(loc=0, scale=1, size=1000)  # Normal distribution
    large_data = np.append(large_data, [5] * 10)  # Introduce clear outliers
    outliers = detect_outliers(list(large_data), mode="high")
    assert 5 in outliers  # Check that one of the artificially high values is detected

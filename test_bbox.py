import pytest
from kimiworker import BoundingBox
from shapely.geometry import Polygon

from bbox import bounding_box_to_polygon, convert_bounding_box, get_intersection_ratio


def test_convert_bounding_box_standard_case():
    """
    Tests conversion of bounding box coordinates from pixels to percentages for a standard case.
    """
    bbox: BoundingBox = convert_bounding_box(
        left=50, top=100, right=150, bottom=200, image_width=200, image_height=400
    )
    expected = {"left": 0.25, "top": 0.25, "right": 0.75, "bottom": 0.5}
    assert bbox == expected, f"Expected {expected}, got {bbox}"


def test_convert_bounding_box_full_image():
    """
    Tests conversion of bounding box coordinates that encompass the entire image.
    """
    bbox: BoundingBox = convert_bounding_box(
        left=0, top=0, right=200, bottom=400, image_width=200, image_height=400
    )
    expected = {"left": 0.0, "top": 0.0, "right": 1.0, "bottom": 1.0}
    assert bbox == expected, f"Expected {expected}, got {bbox}"


def test_convert_bounding_box_with_zero_size():
    """
    Tests conversion of bounding box coordinates where the bounding box has zero size.
    """
    bbox: BoundingBox = convert_bounding_box(
        left=100, top=100, right=100, bottom=100, image_width=200, image_height=200
    )
    expected = {"left": 0.5, "top": 0.5, "right": 0.5, "bottom": 0.5}
    assert bbox == expected, f"Expected {expected}, got {bbox}"


def test_convert_bounding_box_edge_case():
    """
    Tests conversion of bounding box coordinates located at the edge of the image.
    """
    bbox: BoundingBox = convert_bounding_box(
        left=198, top=398, right=200, bottom=400, image_width=200, image_height=400
    )
    expected = {"left": 0.99, "top": 0.995, "right": 1.0, "bottom": 1.0}
    assert bbox == expected, f"Expected {expected}, got {bbox}"


def test_convert_bounding_box_negative_coordinates():
    """
    Tests conversion of bounding box coordinates with negative values.
    """
    bbox: BoundingBox = convert_bounding_box(
        left=-50, top=-100, right=150, bottom=200, image_width=200, image_height=400
    )
    expected = {"left": -0.25, "top": -0.25, "right": 0.75, "bottom": 0.5}
    assert bbox == expected, f"Expected {expected}, got {bbox}"


def test_convert_bounding_box_large_coordinates():
    """
    Tests conversion of bounding box coordinates that extend beyond the image dimensions.
    """
    bbox: BoundingBox = convert_bounding_box(
        left=0, top=0, right=250, bottom=500, image_width=200, image_height=400
    )
    expected = {"left": 0.0, "top": 0.0, "right": 1.25, "bottom": 1.25}
    assert bbox == expected, f"Expected {expected}, got {bbox}"


def test_bounding_box_to_polygon_standard():
    """
    Test conversion of a standard bounding box to a polygon.
    """
    bbox: BoundingBox = {"left": 0.1, "top": 0.1, "right": 0.5, "bottom": 0.5}
    expected_polygon = Polygon([(0.1, 0.1), (0.5, 0.1), (0.5, 0.5), (0.1, 0.5)])
    result_polygon = bounding_box_to_polygon(bbox)
    assert result_polygon.equals(
        expected_polygon
    ), "The polygon does not match the expected shape"


def test_bounding_box_to_polygon_full_range():
    """
    Test conversion of a bounding box that spans the full range of an area.
    """
    bbox: BoundingBox = {"left": 0.0, "top": 0.0, "right": 1.0, "bottom": 1.0}
    expected_polygon = Polygon([(0.0, 0.0), (1.0, 0.0), (1.0, 1.0), (0.0, 1.0)])
    result_polygon = bounding_box_to_polygon(bbox)
    assert result_polygon.equals(
        expected_polygon
    ), "The polygon should cover the full range"


def test_bounding_box_to_polygon_edge_case():
    """
    Test conversion of a bounding box at the edge of the coordinate system.
    """
    bbox: BoundingBox = {"left": 0.0, "top": 0.99, "right": 0.01, "bottom": 1.0}
    expected_polygon = Polygon([(0.0, 0.99), (0.01, 0.99), (0.01, 1.0), (0.0, 1.0)])
    result_polygon = bounding_box_to_polygon(bbox)
    assert result_polygon.equals(
        expected_polygon
    ), "The polygon should match the edge case"


def test_get_intersection_ratio_no_intersection():
    """Test boxes with no overlap return 0.0 intersection ratio"""
    box1: BoundingBox = {"top": 0, "left": 0, "right": 0.5, "bottom": 0.5}
    box2: BoundingBox = {"top": 0.6, "left": 0.6, "right": 1.0, "bottom": 1.0}
    assert get_intersection_ratio(box1, box2) == 0.0


def test_get_intersection_ratio_complete_overlap():
    """Test identical boxes return 1.0 intersection ratio"""
    box1: BoundingBox = {"top": 0, "left": 0, "right": 1, "bottom": 1}
    box2: BoundingBox = {"top": 0, "left": 0, "right": 1, "bottom": 1}
    assert get_intersection_ratio(box1, box2) == 1.0


def test_get_intersection_ratio_partial_overlap():
    """Test partially overlapping boxes return correct intersection ratio"""
    box1: BoundingBox = {"top": 0, "left": 0, "right": 0.6, "bottom": 0.6}
    box2: BoundingBox = {"top": 0.4, "left": 0.4, "right": 1.0, "bottom": 1.0}
    assert pytest.approx(get_intersection_ratio(box1, box2)) == 0.11111111111111106


def test_get_intersection_ratio_one_box_inside_other():
    """Test when one box is fully inside another returns 1.0 intersection ratio"""
    big_box: BoundingBox = {"top": 0, "left": 0, "right": 1, "bottom": 1}
    small_box: BoundingBox = {"top": 0.25, "left": 0.25, "right": 0.75, "bottom": 0.75}
    assert get_intersection_ratio(big_box, small_box) == 1.0


def test_get_intersection_ratio_invalid_box_coords():
    """Test boxes with invalid coordinates (negative values) return 0.0"""
    box1: BoundingBox = {"top": 0, "left": 0, "right": -1, "bottom": 1}
    box2: BoundingBox = {"top": 0, "left": 0, "right": 1, "bottom": 1}
    assert get_intersection_ratio(box1, box2) == 0.0

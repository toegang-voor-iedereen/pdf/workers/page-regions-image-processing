"""
Module for common image processing operations.

This module provides functions for performing various image processing tasks,
such as converting images to grayscale, applying Gaussian blur, thresholding,
detecting edges using the Canny edge detection algorithm, and more.
"""

from typing import Sequence
import cv2
from cv2.typing import MatLike, Scalar, Size
import numpy as np

from kimiworker import BoundingBox
from lines import detect_hough_lines, filter_hough_lines


def grayscale_image(image: MatLike) -> MatLike:
    """
    Convert an image to grayscale.

    Args:
        - image (MatLike): The input image.

    Returns:
        - MatLike: Grayscale version of the input image.
    """
    return (
        image
        if image.ndim == 2 or (image.ndim == 3 and image.shape[2] == 1)
        else cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    )


def blur_image(
    image: MatLike, kernel_size: Size = (7, 7), sigma_x: float = 0
) -> MatLike:
    """
    Apply Gaussian blur to an image.

    Args:
        - image (MatLike): The input image.
        - kernel_size (Size): The size of the kernel used for blurring.
        - sigma_x (float): Standard deviation in the X direction.

    Returns:
        - MatLike: Blurred version of the input image.
    """
    # Apply Gaussian blur to the image using OpenCV's GaussianBlur function
    # GaussianBlur applies a Gaussian filter to the image to reduce noise and blur it
    # 'kernel_size' specifies the size of the kernel used for blurring
    # 'sigma_x' is the standard deviation in the X direction; higher values result in more blur
    return cv2.GaussianBlur(image, kernel_size, sigma_x)


def threshold_image(image: MatLike) -> MatLike:
    """
    Apply thresholding to an image.

    Args:
        - image (MatLike): The input image.

    Returns:
        - MatLike: Thresholded version of the input image.
    """
    # Apply thresholding to the image using OpenCV's threshold function
    # Thresholding is a technique used to separate objects from the background in an image
    # '0' is the threshold value; pixels with intensity greater than this value will be set to 255 (white),
    # and pixels with intensity less than or equal to this value will be set to 0 (black)
    # 'cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU' is a combination of thresholding methods;
    # THRESH_BINARY_INV applies binary inverse thresholding, and THRESH_OTSU automatically computes
    # an optimal threshold value based on the image histogram
    return cv2.threshold(image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]


def dilate_image(
    image: MatLike,
    kernel_shape: int = cv2.MORPH_RECT,
    kernel_size: Size = (5, 3),
) -> MatLike:
    """
    Apply dilation to an image.

    Args:
        - image (MatLike): The input image.
        - kernel_shape (int): The shape of the structuring element kernel used for dilation.
        - kernel_size (Size): The size of the structuring element kernel.

    Returns:
        - MatLike: Dilated version of the input image.
    """
    # Create a structuring element (kernel) for dilation
    # The kernel shape and size determine the pattern of dilation
    # MORPH_RECT creates a rectangular kernel
    # The specified kernel size controls the size of the kernel
    kernel = cv2.getStructuringElement(kernel_shape, kernel_size)

    # Dilate the image using the created kernel
    # 'iterations' parameter controls the number of times dilation is applied
    return cv2.dilate(image, kernel, iterations=4)


def find_contours(
    image: MatLike, mode: int = cv2.RETR_EXTERNAL, method: int = cv2.CHAIN_APPROX_SIMPLE
) -> Sequence[MatLike]:
    """
    Find contours in an image.

    Args:
        - image (MatLike): The input image.
        - mode (int): Contour retrieval mode.
        - method (int): Contour approximation method.

    Returns:
        - Sequence[MatLike]: List of contours found in the image.
    """
    # Find contours in the image using OpenCV's findContours function
    # 'mode' specifies the contour retrieval mode (default: external contours only)
    # 'method' specifies the contour approximation method (default: simple approximation)
    return cv2.findContours(image, mode, method)[0]


def draw_filled_contours(
    image: MatLike, contours: Sequence[MatLike], color: Scalar = (0, 0, 0)
) -> MatLike:
    """
    Draw filled contours on an image.

    Args:
        - image (MatLike): The input image.
        - contours (Sequence[MatLike]): List of contours to be drawn.
        - color (Scalar): Color of the filled contours.

    Returns:
        - MatLike: Image with filled contours drawn.
    """
    # Create a copy of the input image to draw the filled contours on
    with_fill_image = image.copy()

    # Draw filled contours on the copied image
    for contour in contours:
        # Get the bounding rectangle of the contour
        x, y, w, h = cv2.boundingRect(contour)

        # Draw a filled rectangle over the bounding rectangle of the contour
        cv2.rectangle(with_fill_image, (x, y), (x + w, y + h), color, -1)

    return with_fill_image


def canny_edge_image(
    image: MatLike, threshold1: int = 75, threshold2: int = 200
) -> MatLike:
    """
    Apply Canny edge detection to an image.

    Args:
        - image (MatLike): The input image.
        - threshold1 (int): The lower threshold for edge detection.
        - threshold2 (int): The upper threshold for edge detection.

    Returns:
        - MatLike: Image with edges detected using Canny edge detection.
    """
    # The Canny edge detection algorithm identifies edges in an image
    # by detecting significant changes in intensity.
    # It operates in multiple stages, including gradient calculation
    # and thresholding.

    # Apply Canny edge detection to the input image using OpenCV's Canny function.
    # 'threshold1' and 'threshold2' are the thresholds used for edge detection.
    # Pixels with gradient magnitude greater than 'threshold2' are considered strong edges,
    # while pixels with gradient magnitude between 'threshold1' and 'threshold2' are
    # considered weak edges.
    # Pixels with gradient magnitude below 'threshold1' are suppressed and considered
    # as non-edges.
    return cv2.Canny(image, threshold1, threshold2)


def highlight_bold_regions(image: MatLike):
    """
    Apply a series of morphological operations to an image to highlight and extract bold sections.

    This function processes an input image using binary thresholding and morphological transformations
    to isolate and enhance regions of the image with bold features. It utilizes erosion and dilation
    techniques to refine these regions and combine them into a single mask, which is then used to
    highlight these bold sections on the original image.

    Args:
        - image (MatLike): The input image (grayscale)

    Returns:
        - MatLike: A grayscale image where the bold sections are enhanced and highlighted
        against a gray background.
    """

    gray = grayscale_image(image)
    thresh = cv2.threshold(gray, 160, 255, cv2.THRESH_BINARY)[1]
    kernel = np.ones((5, 4), np.uint8)
    kernel2 = np.ones((3, 3), np.uint8)
    marker = cv2.dilate(thresh, kernel, iterations=1)
    mask = cv2.erode(thresh, kernel, iterations=1)

    while True:
        tmp = marker.copy()
        marker = cv2.erode(marker, kernel2)
        marker = cv2.max(mask, marker)
        difference = cv2.subtract(tmp, marker)
        if cv2.countNonZero(difference) == 0:
            break

    marker_color = cv2.cvtColor(marker, cv2.COLOR_GRAY2BGR)
    out = cv2.bitwise_or(image, marker_color)
    return cv2.cvtColor(out, cv2.COLOR_BGR2GRAY)


def get_bold_regions(image: MatLike):
    """
    Converts the contours of bold regions into a list of BoundingBox dicts

    Args:
        - image (MatLike): The input image

    Returns:
        - list[BoundingBox]: A list of BoundingBox dictionaries.
    """

    bounding_boxes: list[BoundingBox] = []
    return bounding_boxes
    # image_width, image_height = get_image_dimensions(image)
    # bold_highlighted = highlight_bold_regions(image)

    # contours = get_image_contours(bold_highlighted)

    # for contour in contours:
    #     left, top, width, height = cv2.boundingRect(contour)
    #     bounding_boxes.append(
    #         convert_bounding_box(
    #             left,
    #             top,
    #             right=left + width,
    #             bottom=top + height,
    #             image_width=image_width,
    #             image_height=image_height,
    #         )
    #     )

    # return bounding_boxes


def load_image(local_file_path: str):
    """
    Safely load the image from a local file path and resizes it if needed.

    Args:
        - local_file_path (str): The file path of the image to be processed.

    Returns:
        - MatLike: Image

    Raises:
        - ValueError: If the image cannot be opened from the provided path.
    """
    image = cv2.imread(local_file_path)
    if image is None:
        raise ValueError(f"Failed to open image: {local_file_path}")

    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    return image


def get_image_dimensions(image: MatLike):
    """
    Gets image width and height

    Args:
        - image (MatLike): A cv2 image.

    Returns:
        - tuple: a tuple containing the images width and height
    """
    image_height, image_width, _ = image.shape
    return image_width, image_height


def get_image_contours(image: MatLike):
    """
    Processes image and returns the detected contours

    Args:
        - image (MatLike): A cv2 image.

    Returns:
        - Sequence[MatLike]: List of contours found in the image.
    """
    gray = grayscale_image(image)
    blur = blur_image(gray)
    threshold = threshold_image(blur)
    dilated_image = dilate_image(threshold)
    return find_contours(dilated_image)


def process_image(local_file_path: str):
    """
    Processes an image from a specified file path by applying various image processing steps.
    The processing steps include converting to grayscale, blurring, thresholding,
    dilating, finding contours, drawing filled contours, and detecting edges using the Canny method.
    Finally, it detects lines using the Hough transform and filters them.

    Args:
        - local_file_path (str): The file path of the image to be processed.

    Returns:
        - tuple: A tuple containing the image and a list of filtered Hough lines.

    Raises:
        - ValueError: If the image cannot be opened from the provided path.
    """
    image = load_image(local_file_path)
    contours = get_image_contours(image)
    with_fill_image = draw_filled_contours(image, contours)
    canny_edged_image = canny_edge_image(with_fill_image)

    lines = detect_hough_lines(canny_edged_image)
    return image, filter_hough_lines(lines)

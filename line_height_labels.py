import numpy as np
from kimiworker import ContentLabel


def get_line_height_labels(
    line_height: float,
    amount_of_line_height_groups: int,
    most_common_avg_line_height_list: np.ndarray,
    largest_line_height_list: np.ndarray,
    least_common_avg_line_height_list: np.ndarray,
):
    """
    Classifies a given line height into categories based on provided lists of line heights.

    This function assigns labels to a line height by checking its presence in lists representing the
    most common, largest, and least common average line heights.

    Args:
        - line_height (float): The line height to classify.
        - amount_of_line_height_groups (int): The total number of distinct line height groups.
        - most_common_avg_line_height_list (np.ndarray): An array of the most common average line heights.
        - largest_line_height_list (np.ndarray): An array of the largest line heights encountered.
        - least_common_avg_line_height_list (np.ndarray): An array of the least common average line heights.

    Returns:
        - List[ContentLabel]: A list of ContentLabels, indicating the classification of the line height.
    """

    labels: list[ContentLabel] = []

    if line_height > 0 and amount_of_line_height_groups > 1:
        if np.any(np.isclose(most_common_avg_line_height_list, line_height)):
            labels.append({"name": "most_common_line_height", "confidence": 1.0})

        if np.any(np.isclose(least_common_avg_line_height_list, line_height)):
            labels.append({"name": "least_common_line_height", "confidence": 1.0})

    if line_height > 0 and np.any(np.isclose(largest_line_height_list, line_height)):
        labels.append({"name": "largest_line_height", "confidence": 1.0})

    return labels
